<?php


    $menu = array(
  0 =>
  array(
    'name' => 'Дорогие товары',
    'icon' => 'star',
    'link' => 'goods.php?price_filter_from=100&price_filter_to=1000000&srch-term=&sort_by=&sort_order=',
    'roles' => 'admin, manager',
    'visible' => 1,
    'group' => '',
    'menu_sort_order' => '10',
    'unauthorized_access' => '0',
  ),
  1 =>
  array(
    'name' => 'Категории',
    'icon' => 'folder',
    'link' => 'categories.php',
    'roles' => 'admin',
    'visible' => 1,
    'group' => 'Админское',
    'menu_sort_order' => '10',
    'unauthorized_access' => '0',
  ),
  2 =>
  array(
    'name' => 'Настройки',
    'icon' => 'cog',
    'link' => 'settings.php',
    'roles' => 'admin',
    'visible' => 1,
    'group' => 'Админское',
    'menu_sort_order' => '10',
    'unauthorized_access' => '0',
  ),
  3 =>
  array(
    'name' => 'Пользователи',
    'icon' => 'user',
    'link' => 'index.php',
    'roles' => 'admin',
    'visible' => 1,
    'group' => 'Админское',
    'menu_sort_order' => '10',
    'unauthorized_access' => '0',
  ),
  4 =>
  array(
    'name' => 'Теги',
    'icon' => 'tags',
    'link' => 'tags.php',
    'roles' => 'admin',
    'visible' => 1,
    'group' => '',
    'menu_sort_order' => '10',
    'unauthorized_access' => '0',
  ),
  5 =>
  array(
    'name' => 'Товары',
    'icon' => 'box-open',
    'link' => 'goods.php',
    'roles' => 'admin, manager',
    'visible' => 1,
    'group' => '',
    'menu_sort_order' => '10',
    'unauthorized_access' => '0',
  ),
  6 =>
  array(
    'name' => 'Товары категории {{#title}}',
    'icon' => 'box-open',
    'link' => 'category_goods.php',
    'roles' => '',
    'visible' => 0,
    'group' => '',
    'menu_sort_order' => '10',
    'unauthorized_access' => '0',
  ),
  7 =>
  array(
    'name' => 'Инструкция',
    'icon' => 'book-open',
    'link' => 'manual.php',
    'roles' => 'admin',
    'visible' => 1,
    'group' => 'Админское',
    'menu_sort_order' => '20',
    'unauthorized_access' => '0',
  ),
);
    $project_name = 'Тестовая админка1';

    $project_wireframe = '0';

    $project_recovery = '0';

    $project_signup = 'none';

    $mysql_user_table = 'users';

    $mysql_user_login = 'login';

    $mysql_user_pass = 'pass';

    $mysql_user_role = 'role';

    $pass_encryption = '';

    $mailman_key = '';

    $smsru_key = '';

    $auth_bg = 'https://image.freepik.com/free-photo/shop-background-interior-shopping-office_1203-3877.jpg';

    $auth_bg_block = 'https://p2.piqsels.com/preview/928/346/457/all-black-background-black-black-and-white.jpg';

    $logo = 'https://image.flaticon.com/icons/png/512/69/69581.png';

    $login_validation = '';

    $auth_page_caption = 'КОНТРОЛЬНАЯ ПАНЕЛЬ';

    $auth_fb = 0;

    $auth_vk = 0;

    $auth_google = 0;

    $menu_search_field = 1;



    $vk_client_id = '7072037';

    $vk_secret = '5ja1q3YvVHwP37Ws2VrC';

    $vk_id_field = '';

    $fb_client_id = '450332879154467';

    $fb_secret = '68e95d5d43840d88e25fdae5280d45fe';

    $fb_id_field = '';

    $google_client_id = '658267997441-44rqgdrefnps6gt233v4oj8vnktc336e.apps.googleusercontent.com';

    $google_secret = 'yrjEZ4fWqXVE-4uHLD2DqlHF';

    $google_id_field = '';


    $soc_avatar_mysql_field= '';

    $soc_email_mysql_field= '';

    $soc_name_mysql_field= '';

    $role_after_social_auth= '';


    $project_tint = '#1253aa';

    $tinypng_key = 'mkFBX6Pq6YRYr6Y6zn9fbMdVTXjgmjg1';

    $generation_date = '2020-11-11';
