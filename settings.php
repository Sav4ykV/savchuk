<?php
    $action = $_REQUEST['action'];
    $actions = [];


    include "engine/core.php";
    include "master-include.php";
    if (!in_array($_SESSION['user']['role'], array(
  0 => 'admin',
)) || $GLOBALS['unauthorized_access']==1) {
        include "menu.php";
        foreach ($menu as $m) {
            $rls = [];
            foreach (explode(",", $m["roles"]) as $r) {
                $rls[] = trim($r);
            }
            if (in_array($_SESSION["user"]["role"], $rls) || $m["unauthorized_access"]==1) {
                header("Location: {$m['link']}");
                die("");
            }
        }

        die("У вас нет доступа");
    }

    class GLOBAL_STORAGE
    {
        public static $parent_object;
    }
    






    


    $actions[''] = function () {
        $id = "1";
        if (isset($id)) {
            $item = q("SELECT * FROM settings WHERE id=?", [$id]);
            $item = $item[0];
            if (function_exists("processData")) {
                $item = processData($item);
            }
        } else {
            die("Ошибка. Редактирование несуществующей записи (вы не указали id)");
        }

        


        $html = '
			
		<style>
			html body.concept, html body.concept header, body.concept .table
			{
				background-color:;
				color:;
			}

			#tableMain tr:nth-child(even)
			{
  				background-color: ;
			}
		</style>
			<h1 style="line-height: 30px"> Редактирование <br /><small>'."Настройки".' #'.$id.'</small></h1>
			
			<form class="form" enctype="multipart/form-data" method="POST">
				
				<fieldset>'.
                    (
                        isset($id)?
                    '<input type="hidden" name="id" value="'.$id.'">
					<input type="hidden" name="action" value="edit_execute">'
                    :
                    '<input type="hidden" name="action" value="create_execute">'
                    )
                    .'

					


				<div class="form-group">
					<label class="control-label" for="textinput">Валюта</label>
					<div>
						<select id="currency" name="currency" class="form-control input-md ">
							
						</select>
					</div>
				</div>

			


								<div class="form-group">
									<label class="control-label" for="textinput">Скидка</label>
									<div>
										<input id="discount" name="discount" type="number" placeholder="" class="form-control input-md "  value="'.htmlspecialchars($item["discount"]).'">
									</div>
								</div>

							

				</fieldset>
				<div>
					<button type="button" class="btn blue-inline" id="edit_page_save">Сохранить</a>
				</div>
			</form>
			

		';

        if (function_exists("processPage")) {
            $html = processPage($html);
        }
        return $html;
    };





    $actions['edit_execute'] = function () {
        $skip = false;
        if (function_exists("allowUpdate")) {
            if (!allowUpdate()) {
                $skip = true;
            }
        }
        if (!$skip) {
            $id = $_REQUEST['id'];
            $set = [];

            $set[] = is_null($_REQUEST['currency'])?"`currency`=NULL":"`currency`='".addslashes($_REQUEST['currency'])."'";

            if (count($set)>0) {
                $set = implode(", ", $set);
                $sql = "UPDATE settings SET $set WHERE id=?";
                if (function_exists("processUpdateQuery")) {
                    $sql = processUpdateQuery($sql);
                }

                qi($sql, [$id]);
                if (function_exists("afterUpdate")) {
                    afterUpdate();
                }
            }
        }
        buildMsg("Изменения сохранены");
        if (isset($_REQUEST['back'])) {
            header("Location: {$_REQUEST['back']}");
        } else {
            header("Location: ".$_SERVER['HTTP_REFERER']);
        }
        die("");
    };







    

    $content = $actions[$action]();
    echo masterRender("Настройки", $content, 2);
