<?php
    $action = $_REQUEST['action'] ?? "";
    $actions = [];

    include "engine/core.php";
    include "master-include.php";

    HeadManager::add('');

    if (!in_array($_SESSION['user']['role'], array(
  0 => '',
)) || $GLOBALS['unauthorized_access']==1) {
        include "menu.php";
        foreach ($menu as $m) {
            $rls = [];
            foreach (explode(",", $m["roles"]) as $r) {
                $rls[] = trim($r);
            }
            if ((in_array($_SESSION["user"]["role"], $rls) || $m["unauthorized_access"]==1) && $m["visible"]==1) {
                header("Location: {$m['link']}");
                die("");
            }
        }

        die("У вас нет доступа");
    }


    class GLOBAL_STORAGE
    {
        public static $parent_object;
    }
    GLOBAL_STORAGE::$parent_object = q1('SELECT * FROM categories WHERE id = ?', ["{$_REQUEST["category_id"]}"]);



    

    define("RPP", 50); //кол-во строк на странице

    function array2csv($array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($df, array_keys($array[0]));
        foreach ($array as $row) {
            fputcsv($df, array_values($row));
        }
        fclose($df);
        return ob_get_clean();
    }

    function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    $actions['csv'] = function () {
        if (function_exists("allowCSV")) {
            if (!allowCSV()) {
                die("У вас нет прав на экспорт CSV");
            }
        }
        download_send_headers("data_export_" . date("Y-m-d") . ".csv");
        $data = get_data(true)[0];

        if (function_exists("processCSV")) {
            $data = processCSV($data);
        }

        echo array2csv($data);
        die();
    };

    $actions['delete_group'] = function () {
        $id = $_REQUEST['id'] ?? 0;
        $res = ["status" => 0];
        qi("DELETE FROM  WHERE id=?", [$id]);
        die(json_encode($res));
    };

    $actions['create_group'] = function () {
        if (strlen(trim($_REQUEST['name'] ?? ""))==0) {
            die(json_encode(["status" => 1, "msg" => "Нельзя создать группу без названия"]));
        }
        
        $res = ["status" => 0, "id" => $new_group_id];
        die(json_encode($res));
    };

    $actions[''] = function () {
        $tags_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM tags', []);
        $tags_values = '['.$tags_values[0]['tags'].']';
        $tags_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM tags '."", []);
        $tags_values = '['.$tags_values[0]['tags'].']';
        $category_id_values = json_encode(q("SELECT title as text, id as value FROM categories", []));
        $category_id_values_text = "";
        foreach (json_decode($category_id_values, true) as $opt) {
            $category_id_values_text.="<option value=\"{$opt['value']}\">{$opt['text']}</option>";
        }

        list($items, $pagination, $cnt) = get_data();

        $sort_order[$_REQUEST['sort_by'] ?? ""] = $_REQUEST['sort_order'] ?? "";

        $next_order['id']='asc';
        $next_order['title']='asc';
        $next_order['descr']='asc';
        $next_order['price']='asc';
        $next_order['category_id']='asc';
        $next_order['gallery']='asc';
        $next_order['tags']='asc';

        if ($_REQUEST['sort_order'] ?? "" =='asc') {
            $sort_icon[$_REQUEST['sort_by'] ?? ""] = '<span class="fa fa-sort-alpha-up" style="margin-left:5px;"></span>';
            $next_order[$_REQUEST['sort_by'] ?? ""] = 'desc';
        } elseif ($_REQUEST['sort_order'] ?? ""=='desc') {
            $sort_icon[$_REQUEST['sort_by'] ?? ""] = '<span class="fa fa-sort-alpha-down" style="margin-left:5px;"></span>';
            $next_order[$_REQUEST['sort_by'] ?? ""] = '';
        } elseif ($_REQUEST['sort_order'] ?? ""=='') {
            $next_order[$_REQUEST['sort_by'] ?? ""] = 'asc';
        }
        $filter_caption = "";
        $show = '
		<script>
				window.onload = function ()
				{
					$(\'.big-icon\').html(\'<i class="fas fa-box-open"></i>\');
				};


		</script>
		
		<style>
			html body.concept, html body.concept header, body.concept .table
			{
				background-color:;
				color:;
			}

			.genesis-text-color
			{
				color:;
			}

			#tableMain div.genesis-item:nth-child(even), #tableMain div.genesis-item:nth-child(even) div.genesis-item-property
			{
  				background-color:  !important;
			}

			body.concept .page-link,
			body.concept .page-link:hover{
				color: ;
			}

			html body.concept, html body.concept header, body.concept .table
			{
				color: ;
			}

		</style>
		<!-- Modal -->
		<div class="modal fade" id="csv_create_modal" role="dialog" aria-labelledby="csvCreateModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="POST">
					<input type="hidden" name="action" value="csv_create_execute">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Массовое добавление записей</h5>
						</div>
						<div class="modal-body">
							<small>Вставьте сюда новые записи. Каждая запись на новой строчке: <b class="csv-create-format">id, Название, Описание, Цена, Фото, Теги</b></small>
							<textarea name="csv"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn cancel" data-dismiss="modal" aria-label="Close">Закрыть</button>
							<button type="submit" class="btn blue-inline" id="csv_create_execute">Сохранить</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="content-header">
			<div class="btn-wrap">
				<h2><a href="#" class="back-btn"><span class="fa fa-arrow-circle-left"></span></a> '."Товары категории ".GLOBAL_STORAGE::$parent_object["title"]."".' </h2>
				<button class="btn blue-inline add_button" data-toggle="modal" data-target="#modal-main">ДОБАВИТЬ</button>
				<p class="small res-cnt">Кол-во результатов: <span class="cnt-number-span">'.$cnt.'</span></p>
			</div>
			
			<a href="#" class="js-extra-filters extra-filters btn blue-inline extra-filters-btn" data-toggle="modal" data-target="#js-extra-filters-modal"><i class="fa fa-filter"></i></a>
			<div class="modal fade" id="js-extra-filters-modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
						<h4 class="modal-title">Дополнительные фильтры</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>

						<div class="modal-body">
						
				<div class="form-group">
    				<label>Категория</label>
					<select class="form-control filter-select category_id-extra-filter" name="category_id_filter" style="width:100%">
					<option value="">----------</option>
					'. $category_id_values_text .'
					</select>
  				</div>
				<script>
					document.addEventListener("DOMContentLoaded", (event) =>
					{
  						$(".category_id-extra-filter").val('.($_REQUEST['category_id_filter'] ?? "''").').trigger("change");
					});
				</script>
			

			<div class="form-group">
				<label>Теги</label>
				<div>
					<input id="tags_extra" name="tags_filter_tags" type="text"  class="form-control input-md filter-tags tags_extra_filter_tags" style="width:100%;">
				</div>
			</div>


					<script>

					  function tags_extra_filter_tags()
					  {
						$("#tags_extra").textext(
						{
						  plugins : "autocomplete tags"
						})
						.bind("getSuggestions", function(e, data)
						{
						  var list = '.$tags_values.',
						  textext = $(e.target).textext()[0],
						  query = (data ? data.query : "") || "";

						  $(this).trigger ("setSuggestions",
						  {
							result : textext.itemManager().filter(list, query)
						  });
						});

					  }


					  if (window.jQuery)
					  {
						tags_extra_filter_tags();
						setTimeout(function()
					   {
						 $(".tags_extra_filter_tags").textext()[0].tags().addTags('.(json_encode(explode(",", $_REQUEST['tags_filter_tags']))).');
					   }, 500);
					  }
					  else
					  {
						window.addEventListener("DOMContentLoaded", function()
						{
						  tags_extra_filter_tags();
						  setTimeout(function()
						   {
							 $(".tags_extra_filter_tags").textext()[0].tags().addTags('.(json_encode(explode(",", $_REQUEST['tags_filter_tags']))).');
						   }, 500);
						});
					  }



					</script>
				  
						</div>

						<div class="modal-footer">
						<button type="button" class="btn cancel" data-dismiss="modal">Отмена</button>
						<button type="button" class="btn btn blue-inline add-filter" data-dismiss="modal">Применить</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div>'.
        ""
        .'</div>';

        $show .= filter_divs();

        $show.='
		
				<div class="genesis-presentation-switch">
					<div class="list-group segmented-control">
						<a href="#" presentation="table" class="list-group-item active"><i class="fa fa-table"></i></a><a href="#" presentation="list" class="list-group-item "><i class="fa fa-bars"></i></a><a href="#" presentation="cards" class="list-group-item "><i class="fa fa-th"></i></a>
					</div>
				</div>';

        $show.='<div class="table-wrap" data-fl-scrolls>';
        $table='
	<div class="data-container genesis-presentation-table  table-clickable-page" id="tableMain">
	<div class="genesis-header">
		<div>
<div class="genesis-header-property"></div>
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>id'. $sort_icon['id'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-from" name="id_filter_from" placeholder="От"/>
							<span class="input-group-btn" style="width:0px;"></span>
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-to" name="id_filter_to" placeholder="До"/>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>', "id") : '
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>id'. $sort_icon['id'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-from" name="id_filter_from" placeholder="От"/>
							<span class="input-group-btn" style="width:0px;"></span>
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-to" name="id_filter_to" placeholder="До"/>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>') . '
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=title&sort_order='. ($next_order['title']) .'\' class=\'sort\' column=\'title\' sort_order=\''.$sort_order['title'].'\'>Название'. $sort_icon['title'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="text" class="form-control filter-text" name="title_filter">
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>', "Название") : '
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=title&sort_order='. ($next_order['title']) .'\' class=\'sort\' column=\'title\' sort_order=\''.$sort_order['title'].'\'>Название'. $sort_icon['title'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="text" class="form-control filter-text" name="title_filter">
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>') . '
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=descr&sort_order='. ($next_order['descr']) .'\' class=\'sort\' column=\'descr\' sort_order=\''.$sort_order['descr'].'\'>Описание'. $sort_icon['descr'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="text" class="form-control filter-text" name="descr_filter">
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>', "Описание") : '
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=descr&sort_order='. ($next_order['descr']) .'\' class=\'sort\' column=\'descr\' sort_order=\''.$sort_order['descr'].'\'>Описание'. $sort_icon['descr'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="text" class="form-control filter-text" name="descr_filter">
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>') . '
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=price&sort_order='. ($next_order['price']) .'\' class=\'sort\' column=\'price\' sort_order=\''.$sort_order['price'].'\'>Цена'. $sort_icon['price'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-from" name="price_filter_from" placeholder="От"/>
							<span class="input-group-btn" style="width:0px;"></span>
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-to" name="price_filter_to" placeholder="До"/>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>', "Цена") : '
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=price&sort_order='. ($next_order['price']) .'\' class=\'sort\' column=\'price\' sort_order=\''.$sort_order['price'].'\'>Цена'. $sort_icon['price'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-from" name="price_filter_from" placeholder="От"/>
							<span class="input-group-btn" style="width:0px;"></span>
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-to" name="price_filter_to" placeholder="До"/>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>') . '

'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				   <a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=gallery&sort_order='. ($next_order['gallery']) .'\' class=\'sort\' column=\'gallery\' sort_order=\''.$sort_order['gallery'].'\'>Фото'. $sort_icon['gallery'].'</a>
			</div>', "Фото") : '
			<div class="genesis-header-property">
				   <a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=gallery&sort_order='. ($next_order['gallery']) .'\' class=\'sort\' column=\'gallery\' sort_order=\''.$sort_order['gallery'].'\'>Фото'. $sort_icon['gallery'].'</a>
			</div>') . '

			<div class="genesis-header-property"></div>
		</div>
</div>
<div class="genesis-tbody">';


        if (count($items) > 0) {
            $agregate = get_agregate();
            foreach ($items as $item) {
                if (isset($item['id'])) {
                    $gallery_images = q("SELECT * FROM `goods_images` WHERE `goods_id` = {$item['id']} ORDER BY `srt`", []);
                } else {
                    $gallery_images = [];
                }
                $gallery_images_html = '';
                foreach ($gallery_images as $img) {
                    $gallery_images_html.="<img src='{$img['img']}'>";
                }
                $tr = "

		<div class='genesis-item' pk='{$item['id']}'>
			<div class='genesis-item-property sortable-handle' style='width:1px;' ><i class='fas fa-bars'></i></div>
			".(function_exists("processTD")?processTD("<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['id']) ? $sort_icon['id'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-from\" name=\"id_filter_from\" placeholder=\"От\"/>
							<span class=\"input-group-btn\" style=\"width:0px;\"></span>
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-to\" name=\"id_filter_to\" placeholder=\"До\"/>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>id:</span>
		</span><div class='js-quotable'>".htmlspecialchars($item['id'])."</div></div>", $item, "id"):"<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['id']) ? $sort_icon['id'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-from\" name=\"id_filter_from\" placeholder=\"От\"/>
							<span class=\"input-group-btn\" style=\"width:0px;\"></span>
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-to\" name=\"id_filter_to\" placeholder=\"До\"/>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>id:</span>
		</span><div class='js-quotable'>".htmlspecialchars($item['id'])."</div></div>")."
".(function_exists("processTD")?processTD("
	<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=title&sort_order='. ($next_order['title']) .'\' class=\'sort\' column=\'title\' sort_order=\''.$sort_order['title'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['title']) ? $sort_icon['title'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"text\" class=\"form-control filter-text\" name=\"title_filter\">
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>Название:</span>
		</span>
		<span class='editable' data-placeholder='' data-inp='text' data-url='engine/ajax.php?action=editable&table=goods' data-pk='{$item['id']}' data-name='title'>".htmlspecialchars($item['title'])."</span>
	</div>", $item, "Название"):"
	<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=title&sort_order='. ($next_order['title']) .'\' class=\'sort\' column=\'title\' sort_order=\''.$sort_order['title'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['title']) ? $sort_icon['title'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"text\" class=\"form-control filter-text\" name=\"title_filter\">
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>Название:</span>
		</span>
		<span class='editable' data-placeholder='' data-inp='text' data-url='engine/ajax.php?action=editable&table=goods' data-pk='{$item['id']}' data-name='title'>".htmlspecialchars($item['title'])."</span>
	</div>")."
".(function_exists("processTD")?processTD("<div class='genesis-item-property '>
			<span class='genesis-attached-column-info'>
				<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=descr&sort_order='. ($next_order['descr']) .'\' class=\'sort\' column=\'descr\' sort_order=\''.$sort_order['descr'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['descr']) ? $sort_icon['descr'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"text\" class=\"form-control filter-text\" name=\"descr_filter\">
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
				<span class='genesis-attached-column-name'>Описание:</span>
			</span><span class='editable' data-placeholder='' data-inp='textarea' data-url='engine/ajax.php?action=editable&table=goods' data-pk='{$item['id']}' data-name='descr'>".htmlspecialchars($item['descr'])."</span></div>", $item, "Описание"):"<div class='genesis-item-property '>
			<span class='genesis-attached-column-info'>
				<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=descr&sort_order='. ($next_order['descr']) .'\' class=\'sort\' column=\'descr\' sort_order=\''.$sort_order['descr'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['descr']) ? $sort_icon['descr'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"text\" class=\"form-control filter-text\" name=\"descr_filter\">
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
				<span class='genesis-attached-column-name'>Описание:</span>
			</span><span class='editable' data-placeholder='' data-inp='textarea' data-url='engine/ajax.php?action=editable&table=goods' data-pk='{$item['id']}' data-name='descr'>".htmlspecialchars($item['descr'])."</span></div>")."
".(function_exists("processTD")?processTD("
		<div class='genesis-item-property '>
			<span class='genesis-attached-column-info'>
				<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=price&sort_order='. ($next_order['price']) .'\' class=\'sort\' column=\'price\' sort_order=\''.$sort_order['price'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['price']) ? $sort_icon['price'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-from\" name=\"price_filter_from\" placeholder=\"От\"/>
							<span class=\"input-group-btn\" style=\"width:0px;\"></span>
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-to\" name=\"price_filter_to\" placeholder=\"До\"/>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
				<span class='genesis-attached-column-name'>Цена:</span>
			</span>
			<span class='editable' data-placeholder='' data-inp='number' data-url='engine/ajax.php?action=editable&table=goods' data-pk='{$item['id']}' data-name='price'>".htmlspecialchars($item['price'])."</span>
		</div>", $item, "Цена"):"
		<div class='genesis-item-property '>
			<span class='genesis-attached-column-info'>
				<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=price&sort_order='. ($next_order['price']) .'\' class=\'sort\' column=\'price\' sort_order=\''.$sort_order['price'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['price']) ? $sort_icon['price'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-from\" name=\"price_filter_from\" placeholder=\"От\"/>
							<span class=\"input-group-btn\" style=\"width:0px;\"></span>
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-to\" name=\"price_filter_to\" placeholder=\"До\"/>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
				<span class='genesis-attached-column-name'>Цена:</span>
			</span>
			<span class='editable' data-placeholder='' data-inp='number' data-url='engine/ajax.php?action=editable&table=goods' data-pk='{$item['id']}' data-name='price'>".htmlspecialchars($item['price'])."</span>
		</div>")."
".(function_exists("processTD")?processTD("


		<div class='genesis-item-property '>
			<span class='genesis-attached-column-info'>
				<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=gallery&sort_order='. ($next_order['gallery']) .'\' class=\'sort\' column=\'gallery\' sort_order=\''.$sort_order['gallery'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['gallery']) ? $sort_icon['gallery'] : '<span class="fa fa-sort"></span>')).'</a>'."</span>
				<span class='genesis-attached-column-name'>Фото:</span>
			</span>".($gallery_images_html!=""?"
				<div class='fotorama' data-fit='contain' data-loop='true' data-allowfullscreen='true' data-width='100%' data-ratio='800/600' data-maxwidth='300' data-maxheight='300'>
					{$gallery_images_html}
				</div>
			":"")."
		</div>", $item, "Фото"):"


		<div class='genesis-item-property '>
			<span class='genesis-attached-column-info'>
				<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=gallery&sort_order='. ($next_order['gallery']) .'\' class=\'sort\' column=\'gallery\' sort_order=\''.$sort_order['gallery'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['gallery']) ? $sort_icon['gallery'] : '<span class="fa fa-sort"></span>')).'</a>'."</span>
				<span class='genesis-attached-column-name'>Фото:</span>
			</span>".($gallery_images_html!=""?"
				<div class='fotorama' data-fit='contain' data-loop='true' data-allowfullscreen='true' data-width='100%' data-ratio='800/600' data-maxwidth='300' data-maxheight='300'>
					{$gallery_images_html}
				</div>
			":"")."
		</div>")."
			<div class='genesis-control-cell'><a href='#' class='edit_btn'><i class='fa fa-edit' style='color:grey;'></i></a> <a href='#' class='delete_btn'><i class='fa fa-trash' style='color:red;'></i></a></div>
		</div>";

                if (function_exists("processTR")) {
                    $tr = processTR($tr, $item);
                }

                $table.=$tr;
            }



            $table .= "</div></div></div>".$pagination;
        } else {
            $table.=' </div></div><div class="empty_table">Нет информации</div>';
        }

        if (function_exists("processTable")) {
            $table = processTable($table);
        }

        $show.=$table;


        $show.="<div></div>".' ';



        $show .= <<<'EOT'
		<style></style>
		<script></script>
EOT;


        return $show;
    };



    $actions['edit'] = function () {
        if (isset($_REQUEST['genesis_edit_id'])) {
            $id = $_REQUEST['genesis_edit_id'];
            $item = q("SELECT * FROM goods WHERE id=?", [$id]);
            $item = $item[0];
        }

        
        if (isset($id)) {
            $gallery_images = q("SELECT * FROM `goods_images` WHERE `goods_id` = $id ORDER BY `srt`", []);
        } else {
            $gallery_images = [];
        }
        $gallery_images_html = '';
        foreach ($gallery_images as $img) {
            $gallery_images_html.="<div class='uploaded-image' style='background:url({$img['img']})' image-id='{$img['id']}'>
			<i class='link-image' data-placement='top' data-content='<p><b>ID изображения:</b> {$img['id']}<br><a href=\"{$img['img']}\">Ссылка для копирования</a></p>'><i class='fa fa-link'></i></i>
			<a href='#' class='remove-image'><i class='fa fa-times'></i></a>
			</div>";
        }
        $tags_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM tags '."", []);
        $tags_values = '['.$tags_values[0]['tags'].']';
        $tags_tm = time();
        $tags_selected_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(b.nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM goods_tags a LEFT JOIN tags b ON (a.tag_id=b.id) WHERE a.goods_id=?', [$item['id']]);
        $tags_selected_values = '['.$tags_selected_values[0]['tags'].']';

        $html = '
			<form class="form" enctype="multipart/form-data" method="POST">
				<fieldset>'.
                    (
                        isset($id)?
                    '<input type="hidden" name="id" value="'.$id.'">
					<input type="hidden" name="action" value="edit_execute">'
                    :
                    '<input type="hidden" name="action" value="create_execute">'
                    )
                    .'

					
		          <div class="form-group not-editable">
		            <label class="control-label" for="textinput">id</label>
		            <div class="js-quotable">
		            <p>
		              '.$item["id"].'
		            </p>
		            </div>
		          </div>

		          


								<div class="form-group">
									<label class="control-label" for="textinput">Название</label>
									<div>
										<input id="title" name="title" type="text"  placeholder="" class="form-control input-md " value="'.htmlspecialchars($item["title"]).'">
									</div>
								</div>

							


							<div class="form-group">
								<label class="control-label" for="textinput">Описание</label>
								<div>
									<textarea id="descr" name="descr" class="form-control input-md  "  >'.htmlspecialchars($item["descr"]).'</textarea>
								</div>
							</div>

						


								<div class="form-group">
									<label class="control-label" for="textinput">Цена</label>
									<div>
										<input id="price" name="price" type="number" placeholder="" class="form-control input-md "  value="'.htmlspecialchars($item["price"]).'">
									</div>
								</div>

							

					<input id="category_id" name="category_id" type="hidden" value="'.htmlspecialchars("{$_REQUEST["category_id"]}").'">
		


					';
        if (isset($id)) {
            $html .= '
						<script>Dropzone.options.dzgallery = false;</script>
						<div class="form-group" class="" >
								<label class="control-label" for="textinput">Фото</label>
								<div class="multiimage-container dropzone-previews dropzone" id="dzgallery" mysql="gallery">'.
                                    $gallery_images_html.'
									<small class="caption">Перетащите сюда изображения</small>
									<small class="add-btn btn btn-default"><span class="fa fa-plus"></span></small>
								</div>
							</div>
							<script>
							function initDropzone()
			                {
			  								Dropzone.options.dzgallery = false;
			  								var global_files_uploaded_right_now = 0;
			  								var global_files_arr = [];
			  								var global_resps_arr = [];
			  								new Dropzone($(\'#dzgallery\')[0],
			  								{
			  									previewsContainer: null,
			  									acceptedFiles: "image/jpeg,image/png,image/gif",
			  									clickable:".add-btn",
			  									url: "?action=upload_gallery&id='.$id.'",
			  									thumbnailWidth: 150,
			  								}).on(\'addedfile\', function()
			  								{
			  									global_files_uploaded_right_now++;
			  								}).on(\'success\', function(file,response)
			  									{
			  										global_files_uploaded_right_now--;
			  										global_files_arr.push(file);
			  										global_resps_arr.push(response);

			  										if(global_files_uploaded_right_now==0)
			  										{
			  											for(var i=0; i<global_files_arr.length; i++)
			  											{

			  												file = global_files_arr[i];
			  												response = global_resps_arr[i];
			  												var obj = JSON.parse(response);
			  												this.removeFile(file);

			  												var str = "\
			  												<div class=\'uploaded-image\' style=\'background:url("+file.dataURL+")\' image-id=\'"+obj.id+"\'> \
			  													<i class=\'link-image\' data-placement=\'top\' data-content=\'\
			  																					<p>\
			  																						<b>ID изображения:</b> "+obj.id+"\
			  																					</p>\
			  																					<p>\
			  																						<a href=\""+obj.url+"\">Ссылка для копирования</a>\
			  																					</p>\'>\
			  														<i class=\'fa fa-link\'></i>\
			  													</i>\
			  													<a href=\'#\' class=\'remove-image\'>\
			  														<i class=\'fa fa-times\'></i>\
			  													</a>\
			  												</div>";

			  												var html = $(str);
			  												$(this.element).append(html);


			  												var items = [];
			  												$(this.element).find(\'.uploaded-image\').each(function()
			  												{
			  													items.push($(this).attr(\'image-id\'));
			  												});
			  												$.get(\'?action=sort_\'+$(this.element).attr(\'mysql\')+\'&items=\'+JSON.stringify(items));
			  											}

			  											startRemovingImages();

			  											global_files_arr = [];
			  											global_resps_arr = [];

			  										}
			  									}).on(\'error\', function (file, error)
			  									{
			  										global_files_uploaded_right_now--;
			  										alert("Не удалосьзагрузить изображение ("+error+") ");
			  										this.removeFile(file);
			  									});
												$(\'.link-image\').popover({html:true});
			                  }

							if (document.readyState === "complete" || document.readyState === "loaded")
							{
								initDropzone();
							}
              				window.addEventListener("DOMContentLoaded", initDropzone);

							</script>';
        }
        $html .='

						


		<div class="form-group">
			<label class="control-label" for="textinput">Теги</label>
			<div class= >
				<input id="tags" name="tags" type="text"  class="form-control input-md tags" style="width:558px; ">
			</div>
		</div>

		<script>

function tags_tags_'.$tags_tm.'()
{
$(".tags").textext(
{
  plugins : "autocomplete tags"
})
.bind("getSuggestions", function(e, data)
{
  var list = '.$tags_values.',
  textext = $(e.target).textext()[0],
  query = (data ? data.query : "") || "";

  $(this).trigger ("setSuggestions",
  {
	result : textext.itemManager().filter(list, query)
  });
});

setTimeout(function()
{
  $(".tags").textext()[0].tags()._formData.forEach(function(i){$(".tags").textext()[0].tags().removeTag(i)});
  $(".tags").textext()[0].tags().addTags('.$tags_selected_values.');
}, 500);
}

if (window.jQuery)
{

tags_tags_'.$tags_tm.'();

}
else
{
window.addEventListener("DOMContentLoaded", function()
{
				$(tags_tags_'.$tags_tm.'());
});
}

		</script>

	
					<div class="text-center not-editable">
						
					</div>

				</fieldset>
			</form>

		';

        if (function_exists("processEditModalHTML")) {
            $html = processEditModalHTML($html);
        }
        die($html);
    };

    $actions['create'] = function () {
        if (isset($id)) {
            $gallery_images = q("SELECT * FROM `goods_images` WHERE `goods_id` = $id ORDER BY `srt`", []);
        } else {
            $gallery_images = [];
        }
        $gallery_images_html = '';
        foreach ($gallery_images as $img) {
            $gallery_images_html.="<div class='uploaded-image' style='background:url({$img['img']})' image-id='{$img['id']}'>
			<i class='link-image' data-placement='top' data-content='<p><b>ID изображения:</b> {$img['id']}<br><a href=\"{$img['img']}\">Ссылка для копирования</a></p>'><i class='fa fa-link'></i></i>
			<a href='#' class='remove-image'><i class='fa fa-times'></i></a>
			</div>";
        }
        $tags_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM tags '."", []);
        $tags_values = '['.$tags_values[0]['tags'].']';
        $tags_tm = time();
        $tags_selected_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(b.nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM goods_tags a LEFT JOIN tags b ON (a.tag_id=b.id) WHERE a.goods_id=?', [$item['id']]);
        $tags_selected_values = '['.$tags_selected_values[0]['tags'].']';

        $html = '
			<form class="form" enctype="multipart/form-data" method="POST">
				<fieldset>
					<input type="hidden" name="action" value="create_execute">
					
		          <div class="form-group not-editable">
		            <label class="control-label" for="textinput">id</label>
		            <div class="js-quotable">
		            <p>
		              '.$item["id"].'
		            </p>
		            </div>
		          </div>

		          


								<div class="form-group">
									<label class="control-label" for="textinput">Название</label>
									<div>
										<input id="title" name="title" type="text"  placeholder="" class="form-control input-md " value="'.htmlspecialchars($item["title"]).'">
									</div>
								</div>

							


							<div class="form-group">
								<label class="control-label" for="textinput">Описание</label>
								<div>
									<textarea id="descr" name="descr" class="form-control input-md  "  >'.htmlspecialchars($item["descr"]).'</textarea>
								</div>
							</div>

						


								<div class="form-group">
									<label class="control-label" for="textinput">Цена</label>
									<div>
										<input id="price" name="price" type="number" placeholder="" class="form-control input-md "  value="'.htmlspecialchars($item["price"]).'">
									</div>
								</div>

							

					<input id="category_id" name="category_id" type="hidden" value="'.htmlspecialchars("{$_REQUEST["category_id"]}").'">
		


					';
        if (isset($id)) {
            $html .= '
						<script>Dropzone.options.dzgallery = false;</script>
						<div class="form-group" class="" >
								<label class="control-label" for="textinput">Фото</label>
								<div class="multiimage-container dropzone-previews dropzone" id="dzgallery" mysql="gallery">'.
                                    $gallery_images_html.'
									<small class="caption">Перетащите сюда изображения</small>
									<small class="add-btn btn btn-default"><span class="fa fa-plus"></span></small>
								</div>
							</div>
							<script>
							function initDropzone()
			                {
			  								Dropzone.options.dzgallery = false;
			  								var global_files_uploaded_right_now = 0;
			  								var global_files_arr = [];
			  								var global_resps_arr = [];
			  								new Dropzone($(\'#dzgallery\')[0],
			  								{
			  									previewsContainer: null,
			  									acceptedFiles: "image/jpeg,image/png,image/gif",
			  									clickable:".add-btn",
			  									url: "?action=upload_gallery&id='.$id.'",
			  									thumbnailWidth: 150,
			  								}).on(\'addedfile\', function()
			  								{
			  									global_files_uploaded_right_now++;
			  								}).on(\'success\', function(file,response)
			  									{
			  										global_files_uploaded_right_now--;
			  										global_files_arr.push(file);
			  										global_resps_arr.push(response);

			  										if(global_files_uploaded_right_now==0)
			  										{
			  											for(var i=0; i<global_files_arr.length; i++)
			  											{

			  												file = global_files_arr[i];
			  												response = global_resps_arr[i];
			  												var obj = JSON.parse(response);
			  												this.removeFile(file);

			  												var str = "\
			  												<div class=\'uploaded-image\' style=\'background:url("+file.dataURL+")\' image-id=\'"+obj.id+"\'> \
			  													<i class=\'link-image\' data-placement=\'top\' data-content=\'\
			  																					<p>\
			  																						<b>ID изображения:</b> "+obj.id+"\
			  																					</p>\
			  																					<p>\
			  																						<a href=\""+obj.url+"\">Ссылка для копирования</a>\
			  																					</p>\'>\
			  														<i class=\'fa fa-link\'></i>\
			  													</i>\
			  													<a href=\'#\' class=\'remove-image\'>\
			  														<i class=\'fa fa-times\'></i>\
			  													</a>\
			  												</div>";

			  												var html = $(str);
			  												$(this.element).append(html);


			  												var items = [];
			  												$(this.element).find(\'.uploaded-image\').each(function()
			  												{
			  													items.push($(this).attr(\'image-id\'));
			  												});
			  												$.get(\'?action=sort_\'+$(this.element).attr(\'mysql\')+\'&items=\'+JSON.stringify(items));
			  											}

			  											startRemovingImages();

			  											global_files_arr = [];
			  											global_resps_arr = [];

			  										}
			  									}).on(\'error\', function (file, error)
			  									{
			  										global_files_uploaded_right_now--;
			  										alert("Не удалосьзагрузить изображение ("+error+") ");
			  										this.removeFile(file);
			  									});
												$(\'.link-image\').popover({html:true});
			                  }

							if (document.readyState === "complete" || document.readyState === "loaded")
							{
								initDropzone();
							}
              				window.addEventListener("DOMContentLoaded", initDropzone);

							</script>';
        }
        $html .='

						


		<div class="form-group">
			<label class="control-label" for="textinput">Теги</label>
			<div class= >
				<input id="tags" name="tags" type="text"  class="form-control input-md tags" style="width:558px; ">
			</div>
		</div>

		<script>

function tags_tags_'.$tags_tm.'()
{
$(".tags").textext(
{
  plugins : "autocomplete tags"
})
.bind("getSuggestions", function(e, data)
{
  var list = '.$tags_values.',
  textext = $(e.target).textext()[0],
  query = (data ? data.query : "") || "";

  $(this).trigger ("setSuggestions",
  {
	result : textext.itemManager().filter(list, query)
  });
});

setTimeout(function()
{
  $(".tags").textext()[0].tags()._formData.forEach(function(i){$(".tags").textext()[0].tags().removeTag(i)});
  $(".tags").textext()[0].tags().addTags('.$tags_selected_values.');
}, 500);
}

if (window.jQuery)
{

tags_tags_'.$tags_tm.'();

}
else
{
window.addEventListener("DOMContentLoaded", function()
{
				$(tags_tags_'.$tags_tm.'());
});
}

		</script>

	
					<div class="text-center not-editable">
						
					</div>
				</fieldset>
			</form>

		';

        if (function_exists("processCreateModalHTML")) {
            $html = processCreateModalHTML($html);
        }
        die($html);
    };


    $actions['edit_page'] = function () {
        if (isset($_REQUEST['genesis_edit_id'])) {
            $id = $_REQUEST['genesis_edit_id'];
            $item = q("SELECT * FROM goods WHERE id=?", [$id]);
            $item = $item[0];
        } else {
            die("Ошибка. Редактирование несуществующей записи (вы не указали id)");
        }

        
        if (isset($id)) {
            $gallery_images = q("SELECT * FROM `goods_images` WHERE `goods_id` = $id ORDER BY `srt`", []);
        } else {
            $gallery_images = [];
        }
        $gallery_images_html = '';
        foreach ($gallery_images as $img) {
            $gallery_images_html.="<div class='uploaded-image' style='background:url({$img['img']})' image-id='{$img['id']}'>
			<i class='link-image' data-placement='top' data-content='<p><b>ID изображения:</b> {$img['id']}<br><a href=\"{$img['img']}\">Ссылка для копирования</a></p>'><i class='fa fa-link'></i></i>
			<a href='#' class='remove-image'><i class='fa fa-times'></i></a>
			</div>";
        }
        $tags_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM tags '."", []);
        $tags_values = '['.$tags_values[0]['tags'].']';
        $tags_tm = time();
        $tags_selected_values = q('SELECT GROUP_CONCAT(CONCAT("\"", REPLACE(b.nm, "\"", "&quot;"), "\"") SEPARATOR ", ") as tags FROM goods_tags a LEFT JOIN tags b ON (a.tag_id=b.id) WHERE a.goods_id=?', [$item['id']]);
        $tags_selected_values = '['.$tags_selected_values[0]['tags'].']';


        $html = '
			<h1 style="line-height: 30px"> Редактирование <br /><small>'."Товары категории ".GLOBAL_STORAGE::$parent_object["title"]."".' #'.$id.'</small></h1>
			<form class="form" enctype="multipart/form-data" method="POST">
				<input type="hidden" name="back" value="'.$_SERVER['HTTP_REFERER'].'">
				<fieldset>'.
                    (
                        isset($id)?
                    '<input type="hidden" name="id" value="'.$id.'">
					<input type="hidden" name="action" value="edit_execute">'
                    :
                    '<input type="hidden" name="action" value="create_execute">'
                    )
                    .'

					
		          <div class="form-group not-editable">
		            <label class="control-label" for="textinput">id</label>
		            <div class="js-quotable">
		            <p>
		              '.$item["id"].'
		            </p>
		            </div>
		          </div>

		          


								<div class="form-group">
									<label class="control-label" for="textinput">Название</label>
									<div>
										<input id="title" name="title" type="text"  placeholder="" class="form-control input-md " value="'.htmlspecialchars($item["title"]).'">
									</div>
								</div>

							


							<div class="form-group">
								<label class="control-label" for="textinput">Описание</label>
								<div>
									<textarea id="descr" name="descr" class="form-control input-md  "  >'.htmlspecialchars($item["descr"]).'</textarea>
								</div>
							</div>

						


								<div class="form-group">
									<label class="control-label" for="textinput">Цена</label>
									<div>
										<input id="price" name="price" type="number" placeholder="" class="form-control input-md "  value="'.htmlspecialchars($item["price"]).'">
									</div>
								</div>

							

					<input id="category_id" name="category_id" type="hidden" value="'.htmlspecialchars("{$_REQUEST["category_id"]}").'">
		


					';
        if (isset($id)) {
            $html .= '
						<script>Dropzone.options.dzgallery = false;</script>
						<div class="form-group" class="" >
								<label class="control-label" for="textinput">Фото</label>
								<div class="multiimage-container dropzone-previews dropzone" id="dzgallery" mysql="gallery">'.
                                    $gallery_images_html.'
									<small class="caption">Перетащите сюда изображения</small>
									<small class="add-btn btn btn-default"><span class="fa fa-plus"></span></small>
								</div>
							</div>
							<script>
							function initDropzone()
			                {
			  								Dropzone.options.dzgallery = false;
			  								var global_files_uploaded_right_now = 0;
			  								var global_files_arr = [];
			  								var global_resps_arr = [];
			  								new Dropzone($(\'#dzgallery\')[0],
			  								{
			  									previewsContainer: null,
			  									acceptedFiles: "image/jpeg,image/png,image/gif",
			  									clickable:".add-btn",
			  									url: "?action=upload_gallery&id='.$id.'",
			  									thumbnailWidth: 150,
			  								}).on(\'addedfile\', function()
			  								{
			  									global_files_uploaded_right_now++;
			  								}).on(\'success\', function(file,response)
			  									{
			  										global_files_uploaded_right_now--;
			  										global_files_arr.push(file);
			  										global_resps_arr.push(response);

			  										if(global_files_uploaded_right_now==0)
			  										{
			  											for(var i=0; i<global_files_arr.length; i++)
			  											{

			  												file = global_files_arr[i];
			  												response = global_resps_arr[i];
			  												var obj = JSON.parse(response);
			  												this.removeFile(file);

			  												var str = "\
			  												<div class=\'uploaded-image\' style=\'background:url("+file.dataURL+")\' image-id=\'"+obj.id+"\'> \
			  													<i class=\'link-image\' data-placement=\'top\' data-content=\'\
			  																					<p>\
			  																						<b>ID изображения:</b> "+obj.id+"\
			  																					</p>\
			  																					<p>\
			  																						<a href=\""+obj.url+"\">Ссылка для копирования</a>\
			  																					</p>\'>\
			  														<i class=\'fa fa-link\'></i>\
			  													</i>\
			  													<a href=\'#\' class=\'remove-image\'>\
			  														<i class=\'fa fa-times\'></i>\
			  													</a>\
			  												</div>";

			  												var html = $(str);
			  												$(this.element).append(html);


			  												var items = [];
			  												$(this.element).find(\'.uploaded-image\').each(function()
			  												{
			  													items.push($(this).attr(\'image-id\'));
			  												});
			  												$.get(\'?action=sort_\'+$(this.element).attr(\'mysql\')+\'&items=\'+JSON.stringify(items));
			  											}

			  											startRemovingImages();

			  											global_files_arr = [];
			  											global_resps_arr = [];

			  										}
			  									}).on(\'error\', function (file, error)
			  									{
			  										global_files_uploaded_right_now--;
			  										alert("Не удалосьзагрузить изображение ("+error+") ");
			  										this.removeFile(file);
			  									});
												$(\'.link-image\').popover({html:true});
			                  }

							if (document.readyState === "complete" || document.readyState === "loaded")
							{
								initDropzone();
							}
              				window.addEventListener("DOMContentLoaded", initDropzone);

							</script>';
        }
        $html .='

						


		<div class="form-group">
			<label class="control-label" for="textinput">Теги</label>
			<div class= >
				<input id="tags" name="tags" type="text"  class="form-control input-md tags" style="width:558px; ">
			</div>
		</div>

		<script>

function tags_tags_'.$tags_tm.'()
{
$(".tags").textext(
{
  plugins : "autocomplete tags"
})
.bind("getSuggestions", function(e, data)
{
  var list = '.$tags_values.',
  textext = $(e.target).textext()[0],
  query = (data ? data.query : "") || "";

  $(this).trigger ("setSuggestions",
  {
	result : textext.itemManager().filter(list, query)
  });
});

setTimeout(function()
{
  $(".tags").textext()[0].tags()._formData.forEach(function(i){$(".tags").textext()[0].tags().removeTag(i)});
  $(".tags").textext()[0].tags().addTags('.$tags_selected_values.');
}, 500);
}

if (window.jQuery)
{

tags_tags_'.$tags_tm.'();

}
else
{
window.addEventListener("DOMContentLoaded", function()
{
				$(tags_tags_'.$tags_tm.'());
});
}

		</script>

	

				</fieldset>
				<div>
					<a href="?'.(http_build_query(array_filter($_REQUEST, function ($k) {
            return !in_array($k, ['action', 'genesis_edit_id']);
        }, ARRAY_FILTER_USE_KEY))).'" class="btn cancel" >Закрыть</a>
					<button type="button" class="btn blue-inline" id="edit_page_save">Сохранить</a>
				</div>
			</form>

		';

        if (function_exists("processEditPageHTML")) {
            $html = processEditPageHTML($html);
        }
        return $html;
    };

    $actions['reorder'] = function () {
        $line = json_decode($_REQUEST['genesis_ids_in_order'] ?? "", true);
        for ($i=0; $i < count($line); $i++) {
            qi("UPDATE `goods` SET `srt` = ? WHERE id = ?", [$i, $line[$i]]);
        }


        die(json_encode(['status'=>0]));
    };



    $actions['change_group'] = function () {
        $id = $_REQUEST['id'] ?? 0;
        $group_id = $_REQUEST['group_id'] ?? 0;
        $prev_group_id = $_REQUEST['prev_group_id'] ?? 0;
        qi("UPDATE `goods` SET `` = ? WHERE id = ?", [$group_id, $id]);

        $agregate = get_agregate($group_id);
        $agregate_html = "";

        $agregate = get_agregate($prev_group_id);
        $prev_agregate_html = "";

        die(json_encode(['status' => 0, 'agregate'=>$agregate_html, 'prev_agregate'=>$prev_agregate_html]));
    };

    $actions['groups_reorder'] = function () {
        $line = json_decode($_REQUEST['genesis_ids_in_order'] ?? "", true);
        for ($i=0; $i < count($line); $i++) {
            qi("UPDATE `` SET `` = ? WHERE id = ?", [$i, $line[$i]]);
        }


        die(json_encode(['status'=>0]));
    };


    $actions['csv_create_execute'] = function () {
        if (function_exists("allowInsert")) {
            if (!allowInsert()) {
                header("Location: ".$_SERVER['HTTP_REFERER']);
                die("");
            }
        }


        $sql = "INSERT IGNORE INTO goods (`title`, `descr`, `price`, `category_id`) VALUES (?, ?, ?, ?)";

        $lines = preg_split("/\r\n|\n|\r/", $_REQUEST['csv'] ?? "");
        $success_count = 0;
        $errors_count = 0;
        foreach ($lines as $line) {
            $line = str_getcsv($line);
            qi($sql, [trim($line[0]), trim($line[1]), trim($line[2]), trim($line[3]), "{$_REQUEST["category_id"]}", trim($line[5]), trim($line[6])]);
            $last_id = qInsertId();
            if ($last_id && $last_id>0) {
                $success_count++;
            } else {
                $errors_count++;
            }

            if (function_exists("afterInsert")) {
                afterInsert($last_id);
            }
        }

        buildMsg(
            ($success_count>0?"Успешно добавлено: {$success_count}<br>":"").
            ($errors_count>0?"Ошибок: {$errors_count}":""),
            $errors_count==0?"success":"danger"
        );





        header("Location: ".$_SERVER['HTTP_REFERER']);
        die("");
    };

    $actions['create_execute'] = function () {
        if (function_exists("allowInsert")) {
            if (!allowInsert()) {
                header("Location: ".$_SERVER['HTTP_REFERER']);
                die("");
            }
        }
        $title = $_REQUEST['title'];
        $descr = $_REQUEST['descr'];
        $price = $_REQUEST['price'];
        $category_id = $_REQUEST['category_id'];

        $params = [$title, $descr, $price, $category_id];
        $sql = "INSERT INTO goods (`title`, `descr`, `price`, `category_id`) VALUES (?, ?, ?, ?)";
        if (function_exists("processInsertQuery")) {
            list($sql, $params) = processInsertQuery($sql, $params);
        }

        qi($sql, array_values($params));
        $last_id = qInsertId();

        if (function_exists("afterInsert")) {
            afterInsert($last_id);
        }

        
        $tags_vals = [];
        $tags_vals2 = [];

        if (count(json_decode($_REQUEST['tags'], true))>0) {
            foreach (json_decode($_REQUEST['tags'], true) as $k) {
                $tags_vals[] = '("'.$k.'")';
                $tags_vals2[] = '"'.$k.'"';
            }
            $tags_vals = implode(", ", $tags_vals);
            $tags_vals2 = implode(", ", $tags_vals2);

            qi("INSERT IGNORE INTO tags (nm) VALUES {$tags_vals}", []);
            qi("INSERT INTO goods_tags (goods_id, tag_id) SELECT $last_id as goods_id, id as tag_id FROM tags WHERE nm IN ({$tags_vals2})", []);
        }
        

        header("Location: ".$_SERVER['HTTP_REFERER']);
        die("");
    };

    $actions['edit_execute'] = function () {
        $skip = false;
        if (function_exists("allowUpdate")) {
            if (!allowUpdate()) {
                $skip = true;
            }
        }
        if (!$skip) {
            $id = $_REQUEST['id'] ?? 0;
            $set = [];

            $set[] = is_null($_REQUEST['title'])?"`title`=NULL":"`title`='".addslashes($_REQUEST['title'])."'";
            $set[] = is_null($_REQUEST['descr'])?"`descr`=NULL":"`descr`='".addslashes($_REQUEST['descr'])."'";
            $set[] = is_null($_REQUEST['price'])?"`price`=NULL":"`price`='".addslashes($_REQUEST['price'])."'";
            $set[] = is_null($_REQUEST['category_id'])?"`category_id`=NULL":"`category_id`='".addslashes($_REQUEST['category_id'])."'";

            $tags_vals = [];
            $tags_vals2 = [];

            qi("DELETE FROM goods_tags WHERE goods_id=?", [$id]);
            if (count(json_decode($_REQUEST['tags'], true))>0) {
                foreach (json_decode($_REQUEST['tags'], true) as $k) {
                    $tags_vals[] = '("'.$k.'")';
                    $tags_vals2[] = '"'.$k.'"';
                }
                $tags_vals = implode(", ", $tags_vals);
                $tags_vals2 = implode(", ", $tags_vals2);

                qi("INSERT IGNORE INTO tags (nm) VALUES {$tags_vals}", []);
                qi("INSERT INTO goods_tags (goods_id, tag_id) SELECT $id as goods_id, id as tag_id FROM tags WHERE nm IN ({$tags_vals2})", []);
            }

        

            if (count($set)>0) {
                $set = implode(", ", $set);
                $sql = "UPDATE goods SET $set WHERE id=?";
                if (function_exists("processUpdateQuery")) {
                    $sql = processUpdateQuery($sql);
                }

                qi($sql, [$id]);
                if (function_exists("afterUpdate")) {
                    afterUpdate($id);
                }
            }
        }

        if (isset($_REQUEST['back'])) {
            header("Location: {$_REQUEST['back']}");
        } else {
            header("Location: ".$_SERVER['HTTP_REFERER']);
        }
        die("");
    };



    $actions['delete'] = function () {
        if (function_exists("allowDelete")) {
            if (!allowDelete()) {
                die("0");
            }
        }

        $id = $_REQUEST['id'] ?? 0;
        try {
            qi("DELETE FROM goods WHERE id=?", [$id]);
            if (function_exists("afterDelete")) {
                afterDelete();
            }
            echo "1";
        } catch (Exception $e) {
            echo "0";
        }

        die("");
    };

    function filter_query($srch)
    {
        $filters = [];
        
        if (isset2($_REQUEST['id_filter_from']) && isset2($_REQUEST['id_filter_to'])) {
            $filters[] = "id >= {$_REQUEST['id_filter_from']} AND id <= {$_REQUEST['id_filter_to']}";
        }

        

        if (isset2($_REQUEST['title_filter'])) {
            $filters[] = "`title` LIKE '%{$_REQUEST['title_filter']}%'";
        }
                

        if (isset2($_REQUEST['descr_filter'])) {
            $filters[] = "`descr` LIKE '%{$_REQUEST['descr_filter']}%'";
        }
                

        if (isset2($_REQUEST['price_filter_from']) && isset2($_REQUEST['price_filter_to'])) {
            $filters[] = "price >= {$_REQUEST['price_filter_from']} AND price <= {$_REQUEST['price_filter_to']}";
        }

        

        if (isset2($_REQUEST['category_id_filter'])) {
            $filters[] = "`category_id` = '{$_REQUEST['category_id_filter']}'";
        }
                

        if (isset2($_REQUEST['tags_filter_tags'])) {
            $tags_filter_tags_comma = implode(', ', array_map(function ($s) {
                return "'$s'";
            }, explode(', ', $_REQUEST['tags_filter_tags'])));
            $tags_filter_tag_ids = q("SELECT id FROM `tags` WHERE nm IN ($tags_filter_tags_comma)", []);
            $tag_flt = [];
            foreach ($tags_filter_tag_ids as $row) {
                $id = $row['id'];
                $tag_flt[] = "(`tags_ids` LIKE '%, $id,%')";
            }
            $filters[] = implode(' AND ', $tag_flt);
        }
                          

        $filter="";
        if (count($filters)>0) {
            $filter = implode(" AND ", $filters);
            if ($srch=="") {
                $filter = " WHERE $filter";
            } else {
                $filter = " AND ($filter)";
            }
        }
        return $filter;
    }

    function filter_divs()
    {
        $category_id_values = json_encode(q("SELECT title as text, id as value FROM categories", []));
        $category_id_values_text = "";
        foreach (json_decode($category_id_values, true) as $opt) {
            $category_id_values_text.="<option value=\"{$opt['value']}\">{$opt['text']}</option>";
        }
        
        if (isset2($_REQUEST['id_filter_from']) && isset2($_REQUEST['id_filter_to'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='id_filter_from' value='{$_REQUEST['id_filter_from']}'>
					<input type='hidden' class='filter' name='id_filter_to' value='{$_REQUEST['id_filter_to']}'>
					<span class='fa fa-times remove-tag'></span> id: <b>{$_REQUEST['id_filter_from']}–{$_REQUEST['id_filter_to']}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }
                

        if (isset2($_REQUEST['title_filter'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='title_filter' value='{$_REQUEST['title_filter']}'>
				   <span class='fa fa-times remove-tag'></span> Название: <b>{$_REQUEST['title_filter']}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }

        

        if (isset2($_REQUEST['descr_filter'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='descr_filter' value='{$_REQUEST['descr_filter']}'>
				   <span class='fa fa-times remove-tag'></span> Описание: <b>{$_REQUEST['descr_filter']}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }

        

        if (isset2($_REQUEST['price_filter_from']) && isset2($_REQUEST['price_filter_to'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='price_filter_from' value='{$_REQUEST['price_filter_from']}'>
					<input type='hidden' class='filter' name='price_filter_to' value='{$_REQUEST['price_filter_to']}'>
					<span class='fa fa-times remove-tag'></span> Цена: <b>{$_REQUEST['price_filter_from']}–{$_REQUEST['price_filter_to']}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }
                

        $text_option = array_filter(json_decode($category_id_values, true), function ($i) {
            return $i['value']==$_REQUEST['category_id_filter'];
        });
        $text_option = array_values($text_option)[0]['text'];
        if (isset2($_REQUEST['category_id_filter'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='category_id_filter' value='{$_REQUEST['category_id_filter']}'>
					<span class='fa fa-times remove-tag'></span> Категория: <b>{$text_option}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }
                

        if (isset2($_REQUEST['tags_filter_tags'])) {
            $filter_divs .= "
          			<div class='filter-tag'>
          					<input type='hidden' class='filter' name='tags_filter_tags' value='{$_REQUEST['tags_filter_tags']}'>
          				   <span class='fa fa-times remove-tag'></span> Теги: <b>{$_REQUEST['tags_filter_tags']}</b>
          			</div>";

            $filter_caption = "Фильтры: ";
        }
        $show = $filter_caption.$filter_divs;

        return $show;
    }

    function get_agregate($group_id = "")
    {
        $items = [];

        $srch = "";
        

        $filter = filter_query($srch);
        $where = "category_id={$_REQUEST["category_id"]}";
        if ($where != "") {
            if ($filter!='' || $srch !='') {
                $where = " AND ($where)";
            } else {
                $where = " WHERE ($where)";
            }
        }

        $group_where = "";
        if (isset($group_id) && !empty($group_id)) {
            $group_id = str_replace("'", "\'", $group_id);
            if ($filter!='' || $srch !='' || $where !='') {
                $group_where = " AND (`` = '{$group_id}')";
            } else {
                $group_where = " WHERE (`` = '{$group_id}')";
            }
        }

        $sql = "SELECT 1 as stub  FROM (SELECT main_table.* , (SELECT GROUP_CONCAT(t.nm SEPARATOR ', ') FROM goods_tags ut LEFT JOIN tags t ON (ut.tag_id=t.id) WHERE ut.goods_id=main_table.id) as tags,
		(SELECT CONCAT(', ', GROUP_CONCAT(t.id SEPARATOR ', '),',') FROM goods_tags ut LEFT JOIN tags t ON (ut.tag_id=t.id) WHERE ut.goods_id=main_table.id) as tags_ids FROM goods main_table) temp $srch $filter $where $group_where $order";

        $debug = (isset($_REQUEST['alef_debug']) && $_REQUEST['alef_debug']==1);
        if (in_array($_SERVER['SERVER_NAME'], ["test-genesis.alef.im", "devtest-genesis.alef.im", "localhost"]) || $debug) {
            if ($_REQUEST['action'] ?? '' == '') {
                echo "<!--SQL AGREGATE {$sql} -->\n";
            }
        }

        $result = q($sql, []);
        return $result[0];
    }

    function get_data($force_kill_pagination=false)
    {
        if (function_exists("allowSelect")) {
            if (!allowSelect()) {
                die("У вас нет доступа к данной странице");
            }
        }

        $pagination = 0;
        if ($force_kill_pagination==true) {
            $pagination = 0;
        }
        $items = [];

        $srch = "";
        

        $filter = filter_query($srch);
        $where = "category_id={$_REQUEST["category_id"]}";
        if ($where != "") {
            if ($filter!='' || $srch !='') {
                $where = " AND ($where)";
            } else {
                $where = " WHERE ($where)";
            }
        }


        
        $default_sort_by = '`srt`';
        $default_sort_order = '';
            

        if (isset($default_sort_by) && $default_sort_by) {
            $order = "ORDER BY $default_sort_by $default_sort_order";
        }

        if (isset($_REQUEST['sort_by']) && $_REQUEST['sort_by']!="") {
            $order = "ORDER BY {$_REQUEST['sort_by']} {$_REQUEST['sort_order']}";
        }

        $debug = (isset($_REQUEST['alef_debug']) && $_REQUEST['alef_debug']==1);
        if ($pagination == 1) {
            $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT  main_table.* , (SELECT GROUP_CONCAT(t.nm SEPARATOR ', ') FROM goods_tags ut LEFT JOIN tags t ON (ut.tag_id=t.id) WHERE ut.goods_id=main_table.id) as tags,
		(SELECT CONCAT(', ', GROUP_CONCAT(t.id SEPARATOR ', '),',') FROM goods_tags ut LEFT JOIN tags t ON (ut.tag_id=t.id) WHERE ut.goods_id=main_table.id) as tags_ids FROM goods main_table) temp $srch $filter $where $order LIMIT :start, :limit";
            if (function_exists("processSelectQuery")) {
                $sql = processSelectQuery($sql);
            }


            if (in_array($_SERVER['SERVER_NAME'], ["test-genesis.alef.im", "devtest-genesis.alef.im", "localhost"]) || $debug) {
                if ($_REQUEST['action' ]?? '' =='') {
                    echo "<!--SQL DATA {$sql} -->\n";
                }
            }

            $items = q(
                $sql,
                [
                    'start' => MAX(($_GET['page']-1), 0)*RPP,
                    'limit' => RPP
                ]
            );
            $cnt = qRows();
            $pagination = pagination($cnt);
        } else {
            $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT main_table.* , (SELECT GROUP_CONCAT(t.nm SEPARATOR ', ') FROM goods_tags ut LEFT JOIN tags t ON (ut.tag_id=t.id) WHERE ut.goods_id=main_table.id) as tags,
		(SELECT CONCAT(', ', GROUP_CONCAT(t.id SEPARATOR ', '),',') FROM goods_tags ut LEFT JOIN tags t ON (ut.tag_id=t.id) WHERE ut.goods_id=main_table.id) as tags_ids FROM goods main_table) temp $srch $filter $where $order";
            if (in_array($_SERVER['SERVER_NAME'], ["test-genesis.alef.im", "devtest-genesis.alef.im", "localhost"]) || $debug) {
                if ($_REQUEST['action'] ?? ""=='') {
                    echo "<!--SQL DATA {$sql} -->";
                }
            }
            if (function_exists("processSelectQuery")) {
                $sql = processSelectQuery($sql);
            }
            $items = q($sql, []);
            $cnt = qRows();
            $pagination = "";
        }

        if (function_exists("processData")) {
            $items = processData($items);
        }

        return [$items, $pagination, $cnt];
    }

    
    $actions["sort_gallery"] = function () {
        $items = json_decode($_REQUEST["items"], true);


        for ($i=0; $i<count($items); $i++) {
            qi("UPDATE `goods_images` SET `srt` = $i WHERE id=?", [$items[$i]]);
        }
        die("");
    };

    $actions["remove_gallery"] = function () {
        $id = $_REQUEST["id"];
        qi("DELETE FROM `goods_images` WHERE id=?", [$id]);
        die("");
    };

    $actions["upload_gallery"] = function () {
        $id = $_REQUEST["id"];
        @mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads');
        chmod($_SERVER['DOCUMENT_ROOT'].'/uploads', 0777);
        if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/uploads')) {
            die("Не удается создать папку uploads в корневой директории. Создайте ее самостоятельно и предоставьте системе доступ к ней.");
        }
        $tm = time();
        $ext = ".".pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
        $target_file = $_SERVER['DOCUMENT_ROOT']."/uploads/".$tm."_".md5($_FILES['file']['name']).$ext;
        move_uploaded_file($_FILES['file']['tmp_name'], $target_file);
        compressImage($target_file);
        $max = q1("SELECT MAX(`srt`) as max FROM `goods_images`", []);
        $max = $max['max']+1;
        qi("INSERT INTO `goods_images` (`goods_id`,  `img`, `srt`) VALUES(?, '".("/uploads/".$tm."_".md5($_FILES['file']['name']).$ext)."', ?)", [$id, $max]);
        $id = qInsertId();

        echo json_encode(["id"=>$id, "url"=>("/uploads/".$tm."_".md5($_FILES['file']['name']).$ext)]);
        die("");
    };




    

    $content = $actions[$action]();
    $finalPage = masterRender("Товары категории ".GLOBAL_STORAGE::$parent_object["title"]."", $content, 6, "genesis-body-presentation-table genesis-grouping-style-");
    if (function_exists("processPage")) {
        $finalPage = processPage($finalPage);
    }
    echo $finalPage;
