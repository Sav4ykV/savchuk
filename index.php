<?php
    $action = $_REQUEST['action'] ?? "";
    $actions = [];

    include "engine/core.php";
    include "master-include.php";

    HeadManager::add('');

    if (!in_array($_SESSION['user']['role'], array(
  0 => 'admin',
)) || $GLOBALS['unauthorized_access']==1) {
        include "menu.php";
        foreach ($menu as $m) {
            $rls = [];
            foreach (explode(",", $m["roles"]) as $r) {
                $rls[] = trim($r);
            }
            if ((in_array($_SESSION["user"]["role"], $rls) || $m["unauthorized_access"]==1) && $m["visible"]==1) {
                header("Location: {$m['link']}");
                die("");
            }
        }

        die("У вас нет доступа");
    }


    class GLOBAL_STORAGE
    {
        public static $parent_object;
    }
    



    

    define("RPP", 2); //кол-во строк на странице

    function array2csv($array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($df, array_keys($array[0]));
        foreach ($array as $row) {
            fputcsv($df, array_values($row));
        }
        fclose($df);
        return ob_get_clean();
    }

    function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    $actions['csv'] = function () {
        if (function_exists("allowCSV")) {
            if (!allowCSV()) {
                die("У вас нет прав на экспорт CSV");
            }
        }
        download_send_headers("data_export_" . date("Y-m-d") . ".csv");
        $data = get_data(true)[0];

        if (function_exists("processCSV")) {
            $data = processCSV($data);
        }

        echo array2csv($data);
        die();
    };

    $actions['delete_group'] = function () {
        $id = $_REQUEST['id'] ?? 0;
        $res = ["status" => 0];
        qi("DELETE FROM  WHERE id=?", [$id]);
        die(json_encode($res));
    };

    $actions['create_group'] = function () {
        if (strlen(trim($_REQUEST['name'] ?? ""))==0) {
            die(json_encode(["status" => 1, "msg" => "Нельзя создать группу без названия"]));
        }
        
        $res = ["status" => 0, "id" => $new_group_id];
        die(json_encode($res));
    };

    $actions[''] = function () {
        $role_values = '[{"text":"Админ", "value":"admin"},{"text":"Менеджер", "value":"manager"}]';
        $role_values_text = "";
        foreach (json_decode($role_values, true) as $opt) {
            $role_values_text.="<option value=\"{$opt['value']}\">{$opt['text']}</option>";
        }
                  

        list($items, $pagination, $cnt) = get_data();

        $sort_order[$_REQUEST['sort_by'] ?? ""] = $_REQUEST['sort_order'] ?? "";

        $next_order['id']='asc';
        $next_order['login']='asc';
        $next_order['pass']='asc';
        $next_order['avatar']='asc';
        $next_order['role']='asc';

        if ($_REQUEST['sort_order'] ?? "" =='asc') {
            $sort_icon[$_REQUEST['sort_by'] ?? ""] = '<span class="fa fa-sort-alpha-up" style="margin-left:5px;"></span>';
            $next_order[$_REQUEST['sort_by'] ?? ""] = 'desc';
        } elseif ($_REQUEST['sort_order'] ?? ""=='desc') {
            $sort_icon[$_REQUEST['sort_by'] ?? ""] = '<span class="fa fa-sort-alpha-down" style="margin-left:5px;"></span>';
            $next_order[$_REQUEST['sort_by'] ?? ""] = '';
        } elseif ($_REQUEST['sort_order'] ?? ""=='') {
            $next_order[$_REQUEST['sort_by'] ?? ""] = 'asc';
        }
        $filter_caption = "";
        $show = '
		<script>
				window.onload = function ()
				{
					$(\'.big-icon\').html(\'<i class="fas fa-user"></i>\');
				};


		</script>
		
		<style>
			html body.concept, html body.concept header, body.concept .table
			{
				background-color:;
				color:;
			}

			.genesis-text-color
			{
				color:;
			}

			#tableMain div.genesis-item:nth-child(even), #tableMain div.genesis-item:nth-child(even) div.genesis-item-property
			{
  				background-color:  !important;
			}

			body.concept .page-link,
			body.concept .page-link:hover{
				color: ;
			}

			html body.concept, html body.concept header, body.concept .table
			{
				color: ;
			}

		</style>
		<!-- Modal -->
		<div class="modal fade" id="csv_create_modal" role="dialog" aria-labelledby="csvCreateModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="POST">
					<input type="hidden" name="action" value="csv_create_execute">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Массовое добавление записей</h5>
						</div>
						<div class="modal-body">
							<small>Вставьте сюда новые записи. Каждая запись на новой строчке: <b class="csv-create-format">id, Логин, Пароль, Аватар, Роль</b></small>
							<textarea name="csv"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn cancel" data-dismiss="modal" aria-label="Close">Закрыть</button>
							<button type="submit" class="btn blue-inline" id="csv_create_execute">Сохранить</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="content-header">
			<div class="btn-wrap">
				<h2><a href="#" class="back-btn"><span class="fa fa-arrow-circle-left"></span></a> '."Пользователи".' </h2>
				<button class="btn blue-inline add_button" data-toggle="modal" data-target="#modal-main">ДОБАВИТЬ</button>
				<p class="small res-cnt">Кол-во результатов: <span class="cnt-number-span">'.$cnt.'</span></p>
			</div>
			
			<form class="navbar-form search-form" role="search">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Поиск" name="srch-term" id="srch-term" value="'.($_REQUEST['srch-term'] ?? "").'">
					<button class="input-group-addon"><i class="fa fa-search"></i></button>
				</div>
			</form>
			
		</div>
		<div>'.
        ""
        .'</div>';

        $show .= filter_divs();

        $show.='
		';

        $show.='<div class="table-wrap" data-fl-scrolls>';
        $table='
	<div class="data-container genesis-presentation-table  table-clickable" id="tableMain">
	<div class="genesis-header">
		<div>
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>id'. $sort_icon['id'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-from" name="id_filter_from" placeholder="От"/>
							<span class="input-group-btn" style="width:0px;"></span>
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-to" name="id_filter_to" placeholder="До"/>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>', "id") : '
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>id'. $sort_icon['id'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-from" name="id_filter_from" placeholder="От"/>
							<span class="input-group-btn" style="width:0px;"></span>
							<input type="number" min="-2147483648" max="2147483648" step="0.01" class="form-control filter-number-to" name="id_filter_to" placeholder="До"/>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>') . '
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=login&sort_order='. ($next_order['login']) .'\' class=\'sort\' column=\'login\' sort_order=\''.$sort_order['login'].'\'>Логин'. $sort_icon['login'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="text" class="form-control filter-text" name="login_filter">
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>', "Логин") : '
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=login&sort_order='. ($next_order['login']) .'\' class=\'sort\' column=\'login\' sort_order=\''.$sort_order['login'].'\'>Логин'. $sort_icon['login'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<input type="text" class="form-control filter-text" name="login_filter">
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>') . '
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				   <a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=pass&sort_order='. ($next_order['pass']) .'\' class=\'sort\' column=\'pass\' sort_order=\''.$sort_order['pass'].'\'>Пароль'. $sort_icon['pass'].'</a>
			</div>', "Пароль") : '
			<div class="genesis-header-property">
				   <a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=pass&sort_order='. ($next_order['pass']) .'\' class=\'sort\' column=\'pass\' sort_order=\''.$sort_order['pass'].'\'>Пароль'. $sort_icon['pass'].'</a>
			</div>') . '
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				   <a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=avatar&sort_order='. ($next_order['avatar']) .'\' class=\'sort\' column=\'avatar\' sort_order=\''.$sort_order['avatar'].'\'>Аватар'. $sort_icon['avatar'].'</a>
			</div>', "Аватар") : '
			<div class="genesis-header-property">
				   <a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=avatar&sort_order='. ($next_order['avatar']) .'\' class=\'sort\' column=\'avatar\' sort_order=\''.$sort_order['avatar'].'\'>Аватар'. $sort_icon['avatar'].'</a>
			</div>') . '
'.(function_exists("processTH")?processTH('
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=role&sort_order='. ($next_order['role']) .'\' class=\'sort\' column=\'role\' sort_order=\''.$sort_order['role'].'\'>Роль'. $sort_icon['role'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<select class="form-control filter-select" name="role_filter">


							'.str_replace(chr(39), '&#39;', $role_values_text).'


							</select>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>', "Роль") : '
			<div class="genesis-header-property">
				<nobr>
					<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=role&sort_order='. ($next_order['role']) .'\' class=\'sort\' column=\'role\' sort_order=\''.$sort_order['role'].'\'>Роль'. $sort_icon['role'].'</a>
					
			<span class=\'fa fa-filter filter btn btn-default\' data-placement=\'bottom\' data-content=\'<div class="input-group">
							<select class="form-control filter-select" name="role_filter">


							'.str_replace(chr(39), '&#39;', $role_values_text).'


							</select>
							<span class="input-group-btn">
								<button class="btn btn-primary add-filter" type="button"><span class="fa fa-filter"></a></button>
							</span>
						</div>\'>
			</span>
				</nobr>
			</div>') . '
			<div class="genesis-header-property"></div>
		</div>
</div>
<div class="genesis-tbody">';


        if (count($items) > 0) {
            $agregate = get_agregate();
            foreach ($items as $item) {
                $tr = "

		<div class='genesis-item' pk='{$item['id']}'>
			
			".(function_exists("processTD")?processTD("<div class='genesis-item-property alef-id-column'>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['id']) ? $sort_icon['id'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-from\" name=\"id_filter_from\" placeholder=\"От\"/>
							<span class=\"input-group-btn\" style=\"width:0px;\"></span>
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-to\" name=\"id_filter_to\" placeholder=\"До\"/>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>id:</span>
		</span><div class='js-quotable'>".htmlspecialchars($item['id'])."</div></div>", $item, "id"):"<div class='genesis-item-property alef-id-column'>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=id&sort_order='. ($next_order['id']) .'\' class=\'sort\' column=\'id\' sort_order=\''.$sort_order['id'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['id']) ? $sort_icon['id'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-from\" name=\"id_filter_from\" placeholder=\"От\"/>
							<span class=\"input-group-btn\" style=\"width:0px;\"></span>
							<input type=\"number\" min=\"-2147483648\" max=\"2147483648\" step=\"0.01\" class=\"form-control filter-number-to\" name=\"id_filter_to\" placeholder=\"До\"/>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>id:</span>
		</span><div class='js-quotable'>".htmlspecialchars($item['id'])."</div></div>")."
".(function_exists("processTD")?processTD("
	<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=login&sort_order='. ($next_order['login']) .'\' class=\'sort\' column=\'login\' sort_order=\''.$sort_order['login'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['login']) ? $sort_icon['login'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"text\" class=\"form-control filter-text\" name=\"login_filter\">
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>Логин:</span>
		</span>
		<span class='editable' data-placeholder='' data-inp='text' data-url='engine/ajax.php?action=editable&table=users' data-pk='{$item['id']}' data-name='login'>".htmlspecialchars($item['login'])."</span>
	</div>", $item, "Логин"):"
	<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=login&sort_order='. ($next_order['login']) .'\' class=\'sort\' column=\'login\' sort_order=\''.$sort_order['login'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['login']) ? $sort_icon['login'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<input type=\"text\" class=\"form-control filter-text\" name=\"login_filter\">
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>Логин:</span>
		</span>
		<span class='editable' data-placeholder='' data-inp='text' data-url='engine/ajax.php?action=editable&table=users' data-pk='{$item['id']}' data-name='login'>".htmlspecialchars($item['login'])."</span>
	</div>")."
".(function_exists("processTD")?processTD("<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=pass&sort_order='. ($next_order['pass']) .'\' class=\'sort\' column=\'pass\' sort_order=\''.$sort_order['pass'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['pass']) ? $sort_icon['pass'] : '<span class="fa fa-sort"></span>')).'</a>'."</span>
			<span class='genesis-attached-column-name'>Пароль:</span>
		</span>
		<span class='editable' data-type='password' data-display=false data-inp='pass'  data-url='engine/ajax.php?action=editable&table=users' data-pk='{$item['id']}' data-name='pass'></span></div>", $item, "Пароль"):"<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=pass&sort_order='. ($next_order['pass']) .'\' class=\'sort\' column=\'pass\' sort_order=\''.$sort_order['pass'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['pass']) ? $sort_icon['pass'] : '<span class="fa fa-sort"></span>')).'</a>'."</span>
			<span class='genesis-attached-column-name'>Пароль:</span>
		</span>
		<span class='editable' data-type='password' data-display=false data-inp='pass'  data-url='engine/ajax.php?action=editable&table=users' data-pk='{$item['id']}' data-name='pass'></span></div>")."
".(function_exists("processTD")?processTD("<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=avatar&sort_order='. ($next_order['avatar']) .'\' class=\'sort\' column=\'avatar\' sort_order=\''.$sort_order['avatar'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['avatar']) ? $sort_icon['avatar'] : '<span class="fa fa-sort"></span>')).'</a>'."</span>
			<span class='genesis-attached-column-name'>Аватар:</span>
		</span>
					". ($item['avatar']?"<a href='#' data-featherlight='{$item['avatar']}'>":"") ."
						<img class='genesis-image js-quotable'
						onerror=\"this.onerror=null; this.src=this.dataset['file-exists']==1?'style/badformat.png':'style/placeholder.jpg'; this.setAttribute('image-url', this.src); this.parentElement.dataset.featherlight = this.src; this.style.objectFit = 'contain';\"
						data-file-exists='".(file_exists($_SERVER['DOCUMENT_ROOT'].'/'.($item['avatar']?$item['avatar']:"style/placeholder.jpg"))?"1":"0")."'
						src='".($item['avatar']?$item['avatar']:"style/placeholder.jpg")."'
						 image-url='".($item['avatar']?$item['avatar']:"style/placeholder.jpg")."' />
					". ($item['avatar']?"</a>":"") ."
				</div>", $item, "Аватар"):"<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=avatar&sort_order='. ($next_order['avatar']) .'\' class=\'sort\' column=\'avatar\' sort_order=\''.$sort_order['avatar'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['avatar']) ? $sort_icon['avatar'] : '<span class="fa fa-sort"></span>')).'</a>'."</span>
			<span class='genesis-attached-column-name'>Аватар:</span>
		</span>
					". ($item['avatar']?"<a href='#' data-featherlight='{$item['avatar']}'>":"") ."
						<img class='genesis-image js-quotable'
						onerror=\"this.onerror=null; this.src=this.dataset['file-exists']==1?'style/badformat.png':'style/placeholder.jpg'; this.setAttribute('image-url', this.src); this.parentElement.dataset.featherlight = this.src; this.style.objectFit = 'contain';\"
						data-file-exists='".(file_exists($_SERVER['DOCUMENT_ROOT'].'/'.($item['avatar']?$item['avatar']:"style/placeholder.jpg"))?"1":"0")."'
						src='".($item['avatar']?$item['avatar']:"style/placeholder.jpg")."'
						 image-url='".($item['avatar']?$item['avatar']:"style/placeholder.jpg")."' />
					". ($item['avatar']?"</a>":"") ."
				</div>")."
".(function_exists("processTD")?processTD("<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=role&sort_order='. ($next_order['role']) .'\' class=\'sort\' column=\'role\' sort_order=\''.$sort_order['role'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['role']) ? $sort_icon['role'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<select class=\"form-control filter-select\" name=\"role_filter\">


							".str_replace(chr(39), '&#39;', $role_values_text)."


							</select>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>Роль:</span>
		</span> <span class=''>".renderRadioGroup("role", $role_values, "users", $item['id'], $item['role'])."</div>", $item, "Роль"):"<div class='genesis-item-property '>
		<span class='genesis-attached-column-info'>
			<span class='buttons-panel'>".'<a href=\'?'.get_query().'&srch-term='.($_REQUEST['srch-term'] ?? "").'&sort_by=role&sort_order='. ($next_order['role']) .'\' class=\'sort\' column=\'role\' sort_order=\''.$sort_order['role'].'\'>'. (str_replace('style="margin-left:5px;"', '', !empty($sort_icon['role']) ? $sort_icon['role'] : '<span class="fa fa-sort"></span>')).'</a>'."
			<span class='fa fa-filter filter ' data-placement='bottom' data-content='<div class=\"input-group\">
							<select class=\"form-control filter-select\" name=\"role_filter\">


							".str_replace(chr(39), '&#39;', $role_values_text)."


							</select>
							<span class=\"input-group-btn\">
								<button class=\"btn btn-primary add-filter\" type=\"button\"><span class=\"fa fa-filter\"></a></button>
							</span>
						</div>'>
			</span></span>
			<span class='genesis-attached-column-name'>Роль:</span>
		</span> <span class=''>".renderRadioGroup("role", $role_values, "users", $item['id'], $item['role'])."</div>")."
			<div class='genesis-control-cell'><a href='#' class='edit_btn'><i class='fa fa-edit' style='color:grey;'></i></a> <a href='#' class='delete_btn'><i class='fa fa-trash' style='color:red;'></i></a></div>
		</div>";

                if (function_exists("processTR")) {
                    $tr = processTR($tr, $item);
                }

                $table.=$tr;
            }



            $table .= "</div></div></div>".$pagination;
        } else {
            $table.=' </div></div><div class="empty_table">Нет информации</div>';
        }

        if (function_exists("processTable")) {
            $table = processTable($table);
        }

        $show.=$table;


        $show.="<div></div>".' ';



        $show .= <<<'EOT'
		<style></style>
		<script></script>
EOT;


        return $show;
    };



    $actions['edit'] = function () {
        if (isset($_REQUEST['genesis_edit_id'])) {
            $id = $_REQUEST['genesis_edit_id'];
            $item = q("SELECT * FROM users WHERE id=?", [$id]);
            $item = $item[0];
        }

        

        $html = '
			<form class="form" enctype="multipart/form-data" method="POST">
				<fieldset>'.
                    (
                        isset($id)?
                    '<input type="hidden" name="id" value="'.$id.'">
					<input type="hidden" name="action" value="edit_execute">'
                    :
                    '<input type="hidden" name="action" value="create_execute">'
                    )
                    .'

					
		          <div class="form-group not-editable">
		            <label class="control-label" for="textinput">id</label>
		            <div class="js-quotable">
		            <p>
		              '.$item["id"].'
		            </p>
		            </div>
		          </div>

		          


								<div class="form-group">
									<label class="control-label" for="textinput">Логин</label>
									<div>
										<input id="login" name="login" type="text"  placeholder="" class="form-control input-md " value="'.htmlspecialchars($item["login"]).'">
									</div>
								</div>

							

	            '. (
                        !isset($id)?'
							<div class="form-group">
								<label class="control-label" for="textinput">Пароль</label>
								<div>
									<input id="pass" name="pass" type="password"  class="form-control input-md " value="">
								</div>
							</div>':
                ''
                    ).'
						


						<div class="form-group form-group-position-relative">
							<label class="control-label" for="textinput">Аватар</label>
							<div class="">
								<img
								onerror="this.onerror=null; this.src=this.dataset[\'file-exists\']==1?\'style/badformat.png\':\'style/placeholder.jpg\'; this.setAttribute(\'image-url\', this.src); this.dataset.featherlight = this.src; this.style.objectFit = \'contain\';"
								data-file-exists="'.((!empty($item["avatar"]))&&(file_exists($_SERVER["DOCUMENT_ROOT"].'/'.$item["avatar"]))?"1":"0").'"
								src="'.$item["avatar"].'" image-url="'.$item["avatar"].'" class="genesis-image js-quotable" style="max-width:200px; max-height:200px;" /><label for="avatar" class="file"> Выберите изображение <input type="file" name="avatar" id="avatar" /></label></div>
						</div>

					



				<div class="form-group">
					<label class="control-label" for="textinput">Роль</label>
					<div>
						<select id="role" name="role" class="form-control input-md ">
							<option value="admin" '.($item["role"]=="admin"?"selected":"").'>Админ</option> 
<option value="manager" '.($item["role"]=="manager"?"selected":"").'>Менеджер</option> 

						</select>
					</div>
				</div>

			
					<div class="text-center not-editable">
						
					</div>

				</fieldset>
			</form>

		';

        if (function_exists("processEditModalHTML")) {
            $html = processEditModalHTML($html);
        }
        die($html);
    };

    $actions['create'] = function () {
        $html = '
			<form class="form" enctype="multipart/form-data" method="POST">
				<fieldset>
					<input type="hidden" name="action" value="create_execute">
					
		          <div class="form-group not-editable">
		            <label class="control-label" for="textinput">id</label>
		            <div class="js-quotable">
		            <p>
		              '.$item["id"].'
		            </p>
		            </div>
		          </div>

		          


								<div class="form-group">
									<label class="control-label" for="textinput">Логин</label>
									<div>
										<input id="login" name="login" type="text"  placeholder="" class="form-control input-md " value="'.htmlspecialchars($item["login"]).'">
									</div>
								</div>

							

	            '. (
            !isset($id)?'
							<div class="form-group">
								<label class="control-label" for="textinput">Пароль</label>
								<div>
									<input id="pass" name="pass" type="password"  class="form-control input-md " value="">
								</div>
							</div>':
                ''
        ).'
						


						<div class="form-group form-group-position-relative">
							<label class="control-label" for="textinput">Аватар</label>
							<div class="">
								<img
								onerror="this.onerror=null; this.src=this.dataset[\'file-exists\']==1?\'style/badformat.png\':\'style/placeholder.jpg\'; this.setAttribute(\'image-url\', this.src); this.dataset.featherlight = this.src; this.style.objectFit = \'contain\';"
								data-file-exists="'.((!empty($item["avatar"]))&&(file_exists($_SERVER["DOCUMENT_ROOT"].'/'.$item["avatar"]))?"1":"0").'"
								src="'.$item["avatar"].'" image-url="'.$item["avatar"].'" class="genesis-image js-quotable" style="max-width:200px; max-height:200px;" /><label for="avatar" class="file"> Выберите изображение <input type="file" name="avatar" id="avatar" /></label></div>
						</div>

					



				<div class="form-group">
					<label class="control-label" for="textinput">Роль</label>
					<div>
						<select id="role" name="role" class="form-control input-md ">
							<option value="admin" '.($item["role"]=="admin"?"selected":"").'>Админ</option> 
<option value="manager" '.($item["role"]=="manager"?"selected":"").'>Менеджер</option> 

						</select>
					</div>
				</div>

			
					<div class="text-center not-editable">
						
					</div>
				</fieldset>
			</form>

		';

        if (function_exists("processCreateModalHTML")) {
            $html = processCreateModalHTML($html);
        }
        die($html);
    };


    $actions['edit_page'] = function () {
        if (isset($_REQUEST['genesis_edit_id'])) {
            $id = $_REQUEST['genesis_edit_id'];
            $item = q("SELECT * FROM users WHERE id=?", [$id]);
            $item = $item[0];
        } else {
            die("Ошибка. Редактирование несуществующей записи (вы не указали id)");
        }

        


        $html = '
			<h1 style="line-height: 30px"> Редактирование <br /><small>'."Пользователи".' #'.$id.'</small></h1>
			<form class="form" enctype="multipart/form-data" method="POST">
				<input type="hidden" name="back" value="'.$_SERVER['HTTP_REFERER'].'">
				<fieldset>'.
                    (
                        isset($id)?
                    '<input type="hidden" name="id" value="'.$id.'">
					<input type="hidden" name="action" value="edit_execute">'
                    :
                    '<input type="hidden" name="action" value="create_execute">'
                    )
                    .'

					
		          <div class="form-group not-editable">
		            <label class="control-label" for="textinput">id</label>
		            <div class="js-quotable">
		            <p>
		              '.$item["id"].'
		            </p>
		            </div>
		          </div>

		          


								<div class="form-group">
									<label class="control-label" for="textinput">Логин</label>
									<div>
										<input id="login" name="login" type="text"  placeholder="" class="form-control input-md " value="'.htmlspecialchars($item["login"]).'">
									</div>
								</div>

							

	            '. (
                        !isset($id)?'
							<div class="form-group">
								<label class="control-label" for="textinput">Пароль</label>
								<div>
									<input id="pass" name="pass" type="password"  class="form-control input-md " value="">
								</div>
							</div>':
                ''
                    ).'
						


						<div class="form-group form-group-position-relative">
							<label class="control-label" for="textinput">Аватар</label>
							<div class="">
								<img
								onerror="this.onerror=null; this.src=this.dataset[\'file-exists\']==1?\'style/badformat.png\':\'style/placeholder.jpg\'; this.setAttribute(\'image-url\', this.src); this.dataset.featherlight = this.src; this.style.objectFit = \'contain\';"
								data-file-exists="'.((!empty($item["avatar"]))&&(file_exists($_SERVER["DOCUMENT_ROOT"].'/'.$item["avatar"]))?"1":"0").'"
								src="'.$item["avatar"].'" image-url="'.$item["avatar"].'" class="genesis-image js-quotable" style="max-width:200px; max-height:200px;" /><label for="avatar" class="file"> Выберите изображение <input type="file" name="avatar" id="avatar" /></label></div>
						</div>

					



				<div class="form-group">
					<label class="control-label" for="textinput">Роль</label>
					<div>
						<select id="role" name="role" class="form-control input-md ">
							<option value="admin" '.($item["role"]=="admin"?"selected":"").'>Админ</option> 
<option value="manager" '.($item["role"]=="manager"?"selected":"").'>Менеджер</option> 

						</select>
					</div>
				</div>

			

				</fieldset>
				<div>
					<a href="?'.(http_build_query(array_filter($_REQUEST, function ($k) {
                    return !in_array($k, ['action', 'genesis_edit_id']);
                }, ARRAY_FILTER_USE_KEY))).'" class="btn cancel" >Закрыть</a>
					<button type="button" class="btn blue-inline" id="edit_page_save">Сохранить</a>
				</div>
			</form>

		';

        if (function_exists("processEditPageHTML")) {
            $html = processEditPageHTML($html);
        }
        return $html;
    };

    $actions['reorder'] = function () {
        $line = json_decode($_REQUEST['genesis_ids_in_order'] ?? "", true);
        for ($i=0; $i < count($line); $i++) {
            qi("UPDATE `users` SET `` = ? WHERE id = ?", [$i, $line[$i]]);
        }


        die(json_encode(['status'=>0]));
    };



    $actions['change_group'] = function () {
        $id = $_REQUEST['id'] ?? 0;
        $group_id = $_REQUEST['group_id'] ?? 0;
        $prev_group_id = $_REQUEST['prev_group_id'] ?? 0;
        qi("UPDATE `users` SET `` = ? WHERE id = ?", [$group_id, $id]);

        $agregate = get_agregate($group_id);
        $agregate_html = "";

        $agregate = get_agregate($prev_group_id);
        $prev_agregate_html = "";

        die(json_encode(['status' => 0, 'agregate'=>$agregate_html, 'prev_agregate'=>$prev_agregate_html]));
    };

    $actions['groups_reorder'] = function () {
        $line = json_decode($_REQUEST['genesis_ids_in_order'] ?? "", true);
        for ($i=0; $i < count($line); $i++) {
            qi("UPDATE `` SET `` = ? WHERE id = ?", [$i, $line[$i]]);
        }


        die(json_encode(['status'=>0]));
    };


    $actions['csv_create_execute'] = function () {
        if (function_exists("allowInsert")) {
            if (!allowInsert()) {
                header("Location: ".$_SERVER['HTTP_REFERER']);
                die("");
            }
        }


        $sql = "INSERT IGNORE INTO users (`login`, `pass`, `avatar`, `role`) VALUES (?, ?, ?, ?)";

        $lines = preg_split("/\r\n|\n|\r/", $_REQUEST['csv'] ?? "");
        $success_count = 0;
        $errors_count = 0;
        foreach ($lines as $line) {
            $line = str_getcsv($line);
            qi($sql, [trim($line[0]), trim($line[1]), trim($line[2]), trim($line[3]), trim($line[4])]);
            $last_id = qInsertId();
            if ($last_id && $last_id>0) {
                $success_count++;
            } else {
                $errors_count++;
            }

            if (function_exists("afterInsert")) {
                afterInsert($last_id);
            }
        }

        buildMsg(
            ($success_count>0?"Успешно добавлено: {$success_count}<br>":"").
            ($errors_count>0?"Ошибок: {$errors_count}":""),
            $errors_count==0?"success":"danger"
        );





        header("Location: ".$_SERVER['HTTP_REFERER']);
        die("");
    };

    $actions['create_execute'] = function () {
        if (function_exists("allowInsert")) {
            if (!allowInsert()) {
                header("Location: ".$_SERVER['HTTP_REFERER']);
                die("");
            }
        }
        $login = $_REQUEST['login'];
        $pass = md5($_REQUEST['pass']);


        $avatar = $_FILES['avatar'];
        if (isset($avatar) && $avatar['name']!=="") {
            $ignore=0;
            @mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads');
            chmod($_SERVER['DOCUMENT_ROOT'].'/uploads', 0777);
            if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/uploads')) {
                die("Не удается создать папку uploads в корневой директории. Создайте ее самостоятельно и предоставьте системе доступ к ней.");
            }
            $tm = time();
            $ext = ".".pathinfo($avatar['name'], PATHINFO_EXTENSION);
            if (in_array($ext, [".phtml", ".php", ".php2", ".php3", ".php4", ".php5", ".php7", ".phps", ".cgi", ".pl", ".asp", ".aspx", ".shtml", ".shtm", ".fcgi", ".fpl", ".htm", ".html"])) {
                $ext = ".txt";
            }
            $target_file = $_SERVER['DOCUMENT_ROOT']."/uploads/".$tm."_".md5($avatar['name']).$ext;
            if (move_uploaded_file($avatar['tmp_name'], $target_file)) {
                compressImage($target_file);
                $avatar = "/uploads/".$tm."_".md5($avatar['name']).$ext;
            } else {
                $set[] = "`avatar`=''";
                buildMsg("Не удается загрузить изображение. Попробуйте отправить файл меньшего размера.", "danger");
            }
        } else {
            $avatar="";
        }


                                
        $role = $_REQUEST['role'];

        $params = [$login, $pass, $avatar, $role];
        $sql = "INSERT INTO users (`login`, `pass`, `avatar`, `role`) VALUES (?, ?, ?, ?)";
        if (function_exists("processInsertQuery")) {
            list($sql, $params) = processInsertQuery($sql, $params);
        }

        qi($sql, array_values($params));
        $last_id = qInsertId();

        if (function_exists("afterInsert")) {
            afterInsert($last_id);
        }

        

        header("Location: ".$_SERVER['HTTP_REFERER']);
        die("");
    };

    $actions['edit_execute'] = function () {
        $skip = false;
        if (function_exists("allowUpdate")) {
            if (!allowUpdate()) {
                $skip = true;
            }
        }
        if (!$skip) {
            $id = $_REQUEST['id'] ?? 0;
            $set = [];

            $set[] = is_null($_REQUEST['login'])?"`login`=NULL":"`login`='".addslashes($_REQUEST['login'])."'";


            $avatar = $_FILES['avatar'];
            if (isset($_FILES['avatar']) && $avatar['name']!=="") {
                $ignore=0;
                @mkdir($_SERVER['DOCUMENT_ROOT'].'/uploads');
                chmod($_SERVER['DOCUMENT_ROOT'].'/uploads', 0777);
                if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/uploads')) {
                    die("Не удается создать папку uploads в корневой директории. Создайте ее самостоятельно и предоставьте системе доступ к ней.");
                }
                $tm = time();
                $ext = ".".pathinfo($avatar['name'], PATHINFO_EXTENSION);
                if (in_array($ext, [".phtml", ".php", ".php2", ".php3", ".php4", ".php5", ".php7", ".phps", ".cgi", ".pl", ".asp", ".aspx", ".shtml", ".shtm", ".fcgi", ".fpl", ".htm", ".html"])) {
                    $ext = ".txt";
                }
                $target_file = $_SERVER['DOCUMENT_ROOT']."/uploads/".$tm."_".md5($avatar['name']).$ext;
                if (move_uploaded_file($avatar['tmp_name'], $target_file)) {
                    compressImage($target_file);
                    $set[] = "`avatar`='".("/uploads/".$tm."_".md5($avatar['name'])).$ext."'";
                } else {
                    $set[] = "`avatar`=''";
                    buildMsg("Не удается загрузить изображение. Попробуйте отправить файл меньшего размера.", "danger");
                }
            } else {
                $avatar = "";
            }

                                    
            $set[] = is_null($_REQUEST['role'])?"`role`=NULL":"`role`='".addslashes($_REQUEST['role'])."'";

            if (count($set)>0) {
                $set = implode(", ", $set);
                $sql = "UPDATE users SET $set WHERE id=?";
                if (function_exists("processUpdateQuery")) {
                    $sql = processUpdateQuery($sql);
                }

                qi($sql, [$id]);
                if (function_exists("afterUpdate")) {
                    afterUpdate($id);
                }
            }
        }

        if (isset($_REQUEST['back'])) {
            header("Location: {$_REQUEST['back']}");
        } else {
            header("Location: ".$_SERVER['HTTP_REFERER']);
        }
        die("");
    };



    $actions['delete'] = function () {
        if (function_exists("allowDelete")) {
            if (!allowDelete()) {
                die("0");
            }
        }

        $id = $_REQUEST['id'] ?? 0;
        try {
            qi("DELETE FROM users WHERE id=?", [$id]);
            if (function_exists("afterDelete")) {
                afterDelete();
            }
            echo "1";
        } catch (Exception $e) {
            echo "0";
        }

        die("");
    };

    function filter_query($srch)
    {
        $filters = [];
        
        if (isset2($_REQUEST['id_filter_from']) && isset2($_REQUEST['id_filter_to'])) {
            $filters[] = "id >= {$_REQUEST['id_filter_from']} AND id <= {$_REQUEST['id_filter_to']}";
        }

        

        if (isset2($_REQUEST['login_filter'])) {
            $filters[] = "`login` LIKE '%{$_REQUEST['login_filter']}%'";
        }
                

        if (isset2($_REQUEST['role_filter'])) {
            $filters[] = "`role` = '{$_REQUEST['role_filter']}'";
        }
                

        $filter="";
        if (count($filters)>0) {
            $filter = implode(" AND ", $filters);
            if ($srch=="") {
                $filter = " WHERE $filter";
            } else {
                $filter = " AND ($filter)";
            }
        }
        return $filter;
    }

    function filter_divs()
    {
        $role_values = '[{"text":"Админ", "value":"admin"},{"text":"Менеджер", "value":"manager"}]';
        $role_values_text = "";
        foreach (json_decode($role_values, true) as $opt) {
            $role_values_text.="<option value=\"{$opt['value']}\">{$opt['text']}</option>";
        }
                  
        
        if (isset2($_REQUEST['id_filter_from']) && isset2($_REQUEST['id_filter_to'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='id_filter_from' value='{$_REQUEST['id_filter_from']}'>
					<input type='hidden' class='filter' name='id_filter_to' value='{$_REQUEST['id_filter_to']}'>
					<span class='fa fa-times remove-tag'></span> id: <b>{$_REQUEST['id_filter_from']}–{$_REQUEST['id_filter_to']}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }
                

        if (isset2($_REQUEST['login_filter'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='login_filter' value='{$_REQUEST['login_filter']}'>
				   <span class='fa fa-times remove-tag'></span> Логин: <b>{$_REQUEST['login_filter']}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }

        

        $text_option = array_filter(json_decode($role_values, true), function ($i) {
            return $i['value']==$_REQUEST['role_filter'];
        });
        $text_option = array_values($text_option)[0]['text'];
        if (isset2($_REQUEST['role_filter'])) {
            $filter_divs .= "
			<div class='filter-tag'>
					<input type='hidden' class='filter' name='role_filter' value='{$_REQUEST['role_filter']}'>
					<span class='fa fa-times remove-tag'></span> Роль: <b>{$text_option}</b>
			</div>";

            $filter_caption = "Фильтры: ";
        }
                
        $show = $filter_caption.$filter_divs;

        return $show;
    }

    function get_agregate($group_id = "")
    {
        $items = [];

        $srch = "";
        
        if (!empty($_REQUEST['srch-term'])) {
            $escaped_srch = str_replace('\'', '\\\'', $_REQUEST['srch-term']);
            $escaped_srch = str_replace('%', '^%', $escaped_srch);
            $escaped_srch = str_replace('_', '^_', $escaped_srch);
            $srch = "WHERE ((`id` LIKE '%".$escaped_srch."%' ESCAPE '^') or (`login` LIKE '%".$escaped_srch."%' ESCAPE '^'))";
            $srch = preg_replace('/\\\\+\'/m', '\\\'', $srch);
        }

        $filter = filter_query($srch);
        $where = "";
        if ($where != "") {
            if ($filter!='' || $srch !='') {
                $where = " AND ($where)";
            } else {
                $where = " WHERE ($where)";
            }
        }

        $group_where = "";
        if (isset($group_id) && !empty($group_id)) {
            $group_id = str_replace("'", "\'", $group_id);
            if ($filter!='' || $srch !='' || $where !='') {
                $group_where = " AND (`` = '{$group_id}')";
            } else {
                $group_where = " WHERE (`` = '{$group_id}')";
            }
        }

        $sql = "SELECT 1 as stub  FROM (SELECT main_table.*  FROM users main_table) temp $srch $filter $where $group_where $order";

        $debug = (isset($_REQUEST['alef_debug']) && $_REQUEST['alef_debug']==1);
        if (in_array($_SERVER['SERVER_NAME'], ["test-genesis.alef.im", "devtest-genesis.alef.im", "localhost"]) || $debug) {
            if ($_REQUEST['action'] ?? '' == '') {
                echo "<!--SQL AGREGATE {$sql} -->\n";
            }
        }

        $result = q($sql, []);
        return $result[0];
    }

    function get_data($force_kill_pagination=false)
    {
        if (function_exists("allowSelect")) {
            if (!allowSelect()) {
                die("У вас нет доступа к данной странице");
            }
        }

        $pagination = 1;
        if ($force_kill_pagination==true) {
            $pagination = 0;
        }
        $items = [];

        $srch = "";
        
        if (!empty($_REQUEST['srch-term'])) {
            $escaped_srch = str_replace('\'', '\\\'', $_REQUEST['srch-term']);
            $escaped_srch = str_replace('%', '^%', $escaped_srch);
            $escaped_srch = str_replace('_', '^_', $escaped_srch);
            $srch = "WHERE ((`id` LIKE '%".$escaped_srch."%' ESCAPE '^') or (`login` LIKE '%".$escaped_srch."%' ESCAPE '^'))";
            $srch = preg_replace('/\\\\+\'/m', '\\\'', $srch);
        }

        $filter = filter_query($srch);
        $where = "";
        if ($where != "") {
            if ($filter!='' || $srch !='') {
                $where = " AND ($where)";
            } else {
                $where = " WHERE ($where)";
            }
        }


        
        $default_sort_by = '';
        $default_sort_order = '';
            

        if (isset($default_sort_by) && $default_sort_by) {
            $order = "ORDER BY $default_sort_by $default_sort_order";
        }

        if (isset($_REQUEST['sort_by']) && $_REQUEST['sort_by']!="") {
            $order = "ORDER BY {$_REQUEST['sort_by']} {$_REQUEST['sort_order']}";
        }

        $debug = (isset($_REQUEST['alef_debug']) && $_REQUEST['alef_debug']==1);
        if ($pagination == 1) {
            $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT  main_table.*  FROM users main_table) temp $srch $filter $where $order LIMIT :start, :limit";
            if (function_exists("processSelectQuery")) {
                $sql = processSelectQuery($sql);
            }


            if (in_array($_SERVER['SERVER_NAME'], ["test-genesis.alef.im", "devtest-genesis.alef.im", "localhost"]) || $debug) {
                if ($_REQUEST['action' ]?? '' =='') {
                    echo "<!--SQL DATA {$sql} -->\n";
                }
            }

            $items = q(
                $sql,
                [
                    'start' => MAX(($_GET['page']-1), 0)*RPP,
                    'limit' => RPP
                ]
            );
            $cnt = qRows();
            $pagination = pagination($cnt);
        } else {
            $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT main_table.*  FROM users main_table) temp $srch $filter $where $order";
            if (in_array($_SERVER['SERVER_NAME'], ["test-genesis.alef.im", "devtest-genesis.alef.im", "localhost"]) || $debug) {
                if ($_REQUEST['action'] ?? ""=='') {
                    echo "<!--SQL DATA {$sql} -->";
                }
            }
            if (function_exists("processSelectQuery")) {
                $sql = processSelectQuery($sql);
            }
            $items = q($sql, []);
            $cnt = qRows();
            $pagination = "";
        }

        if (function_exists("processData")) {
            $items = processData($items);
        }

        return [$items, $pagination, $cnt];
    }

    

    $content = $actions[$action]();
    $finalPage = masterRender("Пользователи", $content, 3, "genesis-body-presentation-table genesis-grouping-style-");
    if (function_exists("processPage")) {
        $finalPage = processPage($finalPage);
    }
    echo $finalPage;
